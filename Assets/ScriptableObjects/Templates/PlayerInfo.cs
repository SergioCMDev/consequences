﻿using UnityEngine;

namespace ScriptableObjects.Templates
{
    [CreateAssetMenu(fileName ="Player", menuName ="PlayerInfo")]
    public class PlayerInfo : ScriptableObject
    {
        public float SpeedMovement;
        // public LayerMask layersToCheckObstacles;
        public LayerMask layersToCheckGround;
    }
}
