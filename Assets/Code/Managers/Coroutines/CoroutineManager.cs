﻿using System.Collections;
using System.Collections.Generic;

namespace Code.Managers.Coroutines
{
    [Prefab(nameof(CoroutineManager), true)]
    public class CoroutineManager: Singleton<CoroutineManager>
    {

        // private Dictionary<IEnumerator, TSignal> _coroutines;
        private List<IEnumerator> _coroutinesList;

        // public void AddCoroutine(IEnumerator coroutine)
        // {
        //     if (_coroutinesList.Contains(coroutine))
        //     {
        //         return;
        //     }
        //     
        //     _coroutinesList.Add(coroutine);
        // }

        public void InitCoroutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }

        // public void InitCoroutine(CoroutineWithEndEvent coroutineWithEndEvent, Func<bool> method)
        // {
        //     coroutineWithEndEvent.OnCoroutineEnd += method;
        //     StartCoroutine(coroutineWithEndEvent);
        //
        // }

   
    }
}