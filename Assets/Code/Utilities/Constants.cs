﻿namespace Code.Utilities
{
    public static class Constants
    {
        public class MovementsRotation
        {
            public const long ROTATION_FORWARD = 90;
            public const long ROTATION_BACK = 270;
            public const long ROTATION_LEFT = 0;
            public const long ROTATION_RIGHT = 180;
        }
        

        public class JsonConstants
        {
            public const string PATH_OF_JSON_SCENE_MODEL_DICTIONARY = "Json/SceneInfoDictionary";
            public const string LEVEL_DATA_PLAYER_INFO = "LevelsDataPlayerInfo";
            public const string LEVEL_INFO = "LevelsInfo";
            public const string LEVEL_LOCK_STATUS_INFO = "LevelsLockStatus";
            public const string PATH_OF_JSON_START_FILE_INFO = "Json/levelInfoStore";
        }

        public class Tutorial
        {
            public const string TUTORIAL_SCENE_NAME = "Tutorial";
            public static string TUTORIAL_VALUE_NAME = "TutorialStatus";
        }

        public class AnimationsValues
        {
            public const string MOVE = "Move";
        }

        public class Prefabs
        {
            public const string STAR_FILLED_PATH = "Prefabs/UI/StarsFilled";
            public const string STAR_EMPTY_PATH = "Prefabs/UI/StarsEmpty";
            public const string LEVEL_BOX = "Prefabs/UI/LevelBox";
            

        }
    }
}