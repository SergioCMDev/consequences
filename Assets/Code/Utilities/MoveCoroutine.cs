﻿using System.Collections;
using UnityEngine;

namespace Code.Utilities
{
    public class MoveCoroutine : IEnumerator
    {
        public event System.Action<MoveCoroutine> OnCoroutineEnd = delegate { };

        public Transform ObjectTransform { get; }
        private Vector3 _originPosition, _destinationPosition;
        private float _movementSpeed;

        public MoveCoroutine(Transform objectTransform, Vector3 originPosition, Vector3 destinationPosition, float movementSpeed)
        {
            ObjectTransform = objectTransform;
            _originPosition = originPosition;
            _destinationPosition = destinationPosition;
            _movementSpeed = movementSpeed;
        }

        public IEnumerator Start()
        {
            float step = (_movementSpeed / (_originPosition - _destinationPosition).magnitude) * Time.fixedDeltaTime;
            float t = 0;
            while (t <= 1.0f)
            {
                t += step; // Goes from 0 to 1, incrementing by step each time
                ObjectTransform.position = Vector3.Lerp(_originPosition, _destinationPosition, t); // Move objectToMove closer to b
                yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
            }
            ObjectTransform.position = _destinationPosition;
            OnCoroutineEnd(this);
        }

        public bool MoveNext()
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public void Reset()
        {
            // throw new System.NotImplementedException();
        }

        public object Current { get; }
    }
}