﻿using Code.Managers.Audio;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Audio;
using UnityEngine.ResourceManagement.AsyncOperations;
using Zenject;

public class AudioMixerPlayer : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;

    [SerializeField] private AudioClip initTheme;
    [SerializeField] private AudioClip clickSound;

    private AudioSource _winAudioSource;
    private AudioSource _startLevelAudioSource;
    private AudioSource _collectStarAudioSource;
    private AudioSource _loseAudioSource;
    private AudioSource _mainThemeInGameAudioSource;
    private AudioSource _mainThemeInMenuAudioSource;
    private AudioSource _clickEffectAudioSource;
    private AudioSource _overButtonEffectAudioSource;

    private bool soundMaster = true, soundEffects = true, soundMusic = true;

    private float volumeMusicStart, volumeEffectsStart;

    public void Initialize()
    {
    }

    [Inject]
    public void Constructor()
    {
        LoadClickAudio();
        LoadInitAudio();
        _audioMixer.GetFloat("MusicVolumeValue", out volumeMusicStart);
        _audioMixer.GetFloat("MusicVolumeValue", out volumeEffectsStart);
        _audioMixer.GetFloat("MusicVolumeValue", out volumeMusicStart);
        Addressables.LoadAssetAsync<AudioClip>("Assets/Sounds/CollectStar.wav").Completed +=
            OnLoadCollectStarSoundComplete;
        Addressables.LoadAssetAsync<AudioClip>("Assets/Sounds/WinSound.wav").Completed += OnLoadWinSoundComplete;
        Addressables.LoadAssetAsync<AudioClip>("Assets/Sounds/LoseSound.wav").Completed += OnLoadLoseSoundComplete;
        Addressables.LoadAssetAsync<AudioClip>("Assets/Sounds/StartLevel.wav").Completed +=
            OnLoadStartLevelSoundComplete;
        Addressables.LoadAssetAsync<AudioClip>("Assets/Sounds/MainTheme_LoopMix.mp3").Completed +=
            OnLoadMainThemeSoundComplete;
        Addressables.LoadAssetAsync<AudioClip>("Assets/Sounds/OverButtonSound.wav").Completed +=
            OnLoadOverButtonSoundComplete;
    }

    #region Load Audios

    private void OnLoadOverButtonSoundComplete(AsyncOperationHandle<AudioClip> obj)
    {
        _overButtonEffectAudioSource = gameObject.AddComponent<AudioSource>();
        _overButtonEffectAudioSource.loop = false;
        _overButtonEffectAudioSource.playOnAwake = false;
        _overButtonEffectAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("SoundEffects")[0];
        _overButtonEffectAudioSource.clip = obj.Result;
    }


    private void OnLoadMainThemeSoundComplete(AsyncOperationHandle<AudioClip> obj)
    {
        _mainThemeInGameAudioSource = gameObject.AddComponent<AudioSource>();
        _mainThemeInGameAudioSource.loop = true;
        _mainThemeInGameAudioSource.playOnAwake = false;
        _mainThemeInGameAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("BackgroundMusic")[0];
        _mainThemeInGameAudioSource.clip = obj.Result;
    }

    private void LoadInitAudio()
    {
        _mainThemeInMenuAudioSource = gameObject.AddComponent<AudioSource>();
        _mainThemeInMenuAudioSource.loop = true;
        _mainThemeInMenuAudioSource.playOnAwake = false;
        _mainThemeInMenuAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("BackgroundMusic")[0];
        _mainThemeInMenuAudioSource.clip = initTheme;
    }

    private void LoadClickAudio()
    {
        _clickEffectAudioSource = gameObject.AddComponent<AudioSource>();
        _clickEffectAudioSource.loop = false;
        _clickEffectAudioSource.playOnAwake = false;
        _clickEffectAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("SoundEffects")[0];
        _clickEffectAudioSource.clip = clickSound;
    }

    private void OnLoadLoseSoundComplete(AsyncOperationHandle<AudioClip> obj)
    {
        _loseAudioSource = gameObject.AddComponent<AudioSource>();
        _loseAudioSource.playOnAwake = false;
        _loseAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("SoundEffects")[0];
        _loseAudioSource.clip = obj.Result;
    }

    private void OnLoadStartLevelSoundComplete(AsyncOperationHandle<AudioClip> obj)
    {
        _startLevelAudioSource = gameObject.AddComponent<AudioSource>();
        _startLevelAudioSource.playOnAwake = false;
        _startLevelAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("SoundEffects")[0];
        _startLevelAudioSource.clip = obj.Result;
    }

    private void OnLoadCollectStarSoundComplete(AsyncOperationHandle<AudioClip> obj)
    {
        _collectStarAudioSource = gameObject.AddComponent<AudioSource>();
        _collectStarAudioSource.playOnAwake = false;
        _collectStarAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("SoundEffects")[0];
        _collectStarAudioSource.clip = obj.Result;
    }


    private void OnLoadWinSoundComplete(AsyncOperationHandle<AudioClip> obj)
    {
        _winAudioSource = gameObject.AddComponent<AudioSource>();
        _winAudioSource.playOnAwake = false;
        _winAudioSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("SoundEffects")[0];
        _winAudioSource.clip = obj.Result;
    }

    #endregion

    public void PlayOverButtonSoundEffect()
    {
        _overButtonEffectAudioSource.Play();
    }

    public void PlayInitMenuBackgroudMusic()
    {
        _mainThemeInMenuAudioSource.Play();
    }

    public void PlayInitGameBackgroudMusic()
    {
        if (_mainThemeInGameAudioSource != null)
            _mainThemeInGameAudioSource.Play();
    }

    public void PlayVictorSound()
    {
        _winAudioSource.Play();
    }

    public void PlayDefeatSound()
    {
        _loseAudioSource.Play();
    }

    public void PlayClickSoundEffect()
    {
        _clickEffectAudioSource.Play();
    }


    private void SetAudioValues()
    {
        _audioMixer.SetFloatAsLinear("BackgroundVolumeValue", soundMaster ? 1 : 0.0f);
        _audioMixer.SetFloatAsLinear("EffectsVolumeValue", soundEffects ? volumeMusicStart : 0.0f);
        _audioMixer.SetFloatAsLinear("MusicVolumeValue", soundMaster ? 1 : 0.0f);
    }

    public void StopInitMenuBackgroundMusic()
    {
        _mainThemeInMenuAudioSource.Stop();
    }

    public void SetAudioState(bool mute)
    {
        soundMaster = !mute;

        SetAudioValues();
    }

    public void StopInitGameBackgroundMusic()
    {
        _mainThemeInGameAudioSource.Stop();
    }

    public void PlayCollectStarSoundEffect()
    {
        if (_collectStarAudioSource != null)
        _collectStarAudioSource.Play();
    }

    public void PlayStartlevelSound()
    {
        _startLevelAudioSource.Play();
    }

    public class Factory : PlaceholderFactory<AudioMixerPlayer>
    {
    }
}