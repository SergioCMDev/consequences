﻿using Code.Infra.Signals.Actions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Levels
{
    public class WinMenuTutorial : MonoBehaviour, IInitializable
    {
        [SerializeField] private Button _restartButton, _exitButton;
    
        [Inject] private SignalBus _signalBus;

        void Start()
        {
            _restartButton.onClick.AddListener(RestartGame);
            _exitButton.onClick.AddListener(ExitLevel);
        }
    
        private void ExitLevel()
        {
            _signalBus.Fire<ExitGameSignal>();
        }

        private void RestartGame()
        {
            _signalBus.Fire<RestartGameSignal>();
        }

        public void Initialize()
        {
            _restartButton.onClick.AddListener(RestartGame);
            _exitButton.onClick.AddListener(ExitLevel);
        }
    }
}
