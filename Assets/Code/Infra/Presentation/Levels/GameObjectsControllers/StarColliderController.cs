﻿using Code.Infra.Models;
using Code.Infra.Signals.Actions;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Levels.GameObjectsControllers
{
    public class StarColliderController : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;

        private void OnTriggerEnter(Collider other)
        {
            _signalBus.Fire(new PlayerGetStarSignal { });
            gameObject.SetActive(false);
        }
    }
}