﻿using Code.Infra.Models;
using Code.Infra.Signals.Actions;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Levels.GameObjectsControllers
{
    public class GoalTriggerBehaviour : MonoBehaviour
    {
        [SerializeField] private bool hasPushed;
        [Inject] protected SignalBus _signalBus;
        
        private void OnTriggerEnter(Collider collision)
        {
            PlayerBase player = collision.GetComponent<PlayerBase>();
            if (!hasPushed && player)
            {
                hasPushed = true;
                _signalBus.Fire(new PlayerReachesGoalSignal() {PlayerBase = player});
            }
        }

        private void OnTriggerExit(Collider other)
        {
            PlayerBase player = other.GetComponent<PlayerBase>();

            if (player)
            {
                hasPushed = false;
                _signalBus.Fire(new PlayerLeavesGoalSignal() {PlayerBase = player});
            }
        }
    }
}