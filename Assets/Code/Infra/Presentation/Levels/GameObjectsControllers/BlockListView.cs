﻿using System.Collections.Generic;
using Code.Infra.Signals.Actions;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Levels.GameObjectsControllers
{
    public class BlockListView : MonoBehaviour
    {
        [SerializeField] private List<GameObject> _blocks;
        [Inject] private SignalBus _signalBus;

        void Start()
        {
            foreach (GameObject block in _blocks)
            {
                _signalBus.Fire(new AddBockPositionSignal()
                {
                    BlockPositionToAdd = block.transform.position
                });

            }
        }
    }
}
