﻿using System;
using UnityEngine;

namespace Code.Infra.Presentation.Presenters
{
    public class GroundPushBehaviour : MonoBehaviour
    {
        public event Action<Vector3> OnPlayerPushGroundCube = delegate { };
        private GameObject _gameObject;

        public void OnMouseDown()
        {
            OnPlayerPushGroundCube.Invoke(gameObject.transform.position);
        }
    }
}