﻿using Code.Infra.Models;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Movements;

namespace Code.Infra.Presentation.Presenters
{
    public class PlayerTwoMovementBehaviour : PlayerBase
    {
        private void Start()
        {
            _signalBus.Fire(new PlayerTwoInitializedSignal() {InitialPosition = transform.position});
            _signalBus.Subscribe<PlayerTwoMoveSignal>(SetPlayerMove);
            _signalBus.Subscribe<CalculateNextMovePlayerTwoSignal>(CalculateNexMovePlayer);
        }
        
        private void CalculateNexMovePlayer(CalculateNextMovePlayerTwoSignal signal)
        {
            TypeMovement typeMovement = GetReverseMove(signal.TypeMovement);
            CheckPositionsToMove(signal.NumMoves, typeMovement);
            
            _signalBus.Fire(new RetrieveMovementsToDoBasedOnPlayerTwoSignal()
                {InfoMovement = signal.TypeMovement, NumMovesPlayerOne = signal.NumMoves});
        }

        private void SetPlayerMove(PlayerTwoMoveSignal playerOneMoveSignal)
        {
            base.SetPlayerMove(playerOneMoveSignal.TypeMovement);
        }
    }
}