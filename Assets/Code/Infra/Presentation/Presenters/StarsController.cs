﻿using Code.Infra.Interfaces;
using Code.Infra.Presentation.Views;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using Zenject;

namespace Code.Infra.Presentation.Presenters
{
    public class StarsController : IInitializable
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private IStarModel _starModel;
        [Inject] private StarCounterView _starCounterView;

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerGetStarSignal>(PlayerGetStar);
            _signalBus.Subscribe<RestartGameSignal>(RestartGame);
        }

        private void RestartGame(RestartGameSignal obj)
        {
            _starModel.Initialize();
        }

        private void Dispose()
        {
            _signalBus.Unsubscribe<PlayerGetStarSignal>(PlayerGetStar);
        }

        private void PlayerGetStar(PlayerGetStarSignal signal)
        {
            _starModel.UpdateCounter();
            _signalBus.Fire<PlayCollectStarSoundSignal>();

            _starCounterView.UpdateCounter(_starModel.GetStars());
        }
    }
}
