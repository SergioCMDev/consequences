﻿using System;
using Code.Infra.Models;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Movements;

namespace Code.Infra.Presentation.Presenters
{
    public class PlayerOneMovementBehaviour : PlayerBase
    {
        public void Start()
        {
            _signalBus.Subscribe<PlayerOneMoveSignal>(SetPlayerMove);
            _signalBus.Subscribe<CalculateNextMovePlayerOneSignal>(CalculateNexMovePlayer);
            _signalBus.Fire(new PlayerOneInitializedSignal() {InitialPosition = transform.position});
        }

        private void CalculateNexMovePlayer(CalculateNextMovePlayerOneSignal obj)
        {
            CheckPositionsToMove(obj.NumMoves, obj.TypeMovement);

            if (_movesToDO.Count > 0)
            {
                int numMoves = Math.Abs(GetNumOfMoves(obj.TypeMovement, _movesToDO[_movesToDO.Count - 1]));
                _signalBus.Fire(new CalculateNextMovePlayerTwoSignal()
                    {TypeMovement = obj.TypeMovement, NumMoves = numMoves});
            }
        }

        private void SetPlayerMove(PlayerOneMoveSignal playerOneMoveSignal)
        {
            base.SetPlayerMove(playerOneMoveSignal.TypeMovement);
        }


    }
}