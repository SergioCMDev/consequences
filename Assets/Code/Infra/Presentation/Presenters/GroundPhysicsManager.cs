﻿using System.Collections.Generic;
using Code.Infra.Signals.Actions;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Presenters
{
    public class GroundPhysicsManager : MonoBehaviour
    {
        [SerializeField] private List<GroundPushBehaviour> _groundTriggerList;

        [Inject] private SignalBus _signalBus;

        void Start()
        {
        
            foreach (var groundBlock in _groundTriggerList)
            {
                if (groundBlock != null)
                {
                    groundBlock.OnPlayerPushGroundCube += SendNewCubeToMove;
                }
            }
        }

        private void SendNewCubeToMove(Vector3 obj)
        {
            _signalBus.Fire(new MoveToNewCubeSignal() {CubePosition = obj});
        }

        void OnDestroy()
        {
            foreach (var groundBlock in _groundTriggerList)
            {
                if (groundBlock != null)
                {
                    groundBlock.OnPlayerPushGroundCube -= SendNewCubeToMove;
                }
            }
        }
    }
}