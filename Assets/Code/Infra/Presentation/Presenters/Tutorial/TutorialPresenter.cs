﻿using Code.Infra.Interfaces;
using Code.Infra.Presentation.Views;
using Code.Infra.Signals.Actions;
using Zenject;

namespace Code.Infra.Presentation.Presenters.Tutorial
{
    public class TutorialPresenter : IInitializable
    {
        [Inject] private ITutorialModel _tutorialModel;
        [Inject] private SignalBus _signalBus;
        [Inject] private TutorialView _tutorialView;

        public void Initialize()
        {
            _signalBus.Subscribe<TutorialCompletedSignal>(TutorialDone);
            _signalBus.Fire(new DisablePlayerMovementSignal());
            _tutorialView.OnButtonNextInitTutorialClicked += ChangeToKatWindow;
            _tutorialView.OnButtonNextKatInfoClicked += ChangeToNekoWindow;
            _tutorialView.OnButtonNextNekoInfoClicked += ChangeToMovementsWindow;
            _tutorialView.OnButtonNextMovementsInfoClicked += ChangeToActivatorsWindow;
            _tutorialView.OnButtonNextActivatorsInfoClicked += ChangeToGoalsWindow;
            _tutorialView.OnButtonNextGoalsInfoClicked += ChangeToStartPlayWindow;
            _tutorialView.OnButtonStartGameInfoClicked += StartGame;
        }

        private void StartGame()
        {
            _tutorialView.CloseAllWindows();
            _signalBus.Fire(new EndTutorialExplicationSignal());
        }

        private void ChangeToStartPlayWindow()
        {
            _tutorialView.ChangeToStartPlayWindow();
        }

        private void ChangeToGoalsWindow()
        {
            _tutorialView.ChangeToGoalsWindow();
        }

        private void ChangeToActivatorsWindow()
        {
            _tutorialView.ChangeToActivatorsWindow();
        }

        private void ChangeToMovementsWindow()
        {
            _tutorialView.ChangeToMovementsWindow();
        }

        private void ChangeToNekoWindow()
        {
            _tutorialView.ChangeToIntroNekoWindow();
        }

        private void ChangeToKatWindow()
        {
            _tutorialView.ChangeToIntroKatWindow();
        }

        private void TutorialDone(TutorialCompletedSignal obj)
        {
            _tutorialModel.SetTutorialDone();
            _tutorialView.ShowVictoryWindow();

        }
    }
}