﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Code.Infra.Interfaces;
using Code.Infra.Models;
using Code.Infra.Presentation.Views;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Movements;
using Code.Infra.Signals.UpdateViews;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Presenters
{
    //TODO SEPARATE THIS CLASS
    public class PlayersMovementController : IInitializable
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private MovementsCounterView _movementsCounterView;
        [Inject] private IMovementsModel _movementsModel;

        [Inject] private IBlockStatusMovementModel _blockMovementModel;

        [Inject] private IPlayersPositionsModel _playersPositionsModel;
        private List<PlayerBase> _playersStopped = new List<PlayerBase>();
        private const int NUM_PLAYERS = 2;

        private bool _winGame = false;

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerOneInitializedSignal>(PlayerOneInitialized);
            _signalBus.Subscribe<PlayerTwoInitializedSignal>(PlayerTwoInitialized);
            _signalBus.Subscribe<MoveForwardSignal>(InitMoveForward);
            _signalBus.Subscribe<MoveBackwardSignal>(InitMoveBackward);
            _signalBus.Subscribe<MoveRightSignal>(InitMoveRight);
            _signalBus.Subscribe<MoveLeftSignal>(InitMoveLeft);
            _signalBus.Subscribe<RestartGameSignal>(RestartGame);
            _signalBus.Subscribe<DisablePlayerMovementSignal>(DisablePlayerMovement);
            _signalBus.Subscribe<EndTutorialExplicationSignal>(EnablePlayerMovement);
            _signalBus.Subscribe<MoveToNewCubeSignal>(MoveToNewCubeSignal);
            _signalBus.Subscribe<RetrieveMovementsToDoBasedOnPlayerTwoSignal>(UpdateMoveIfNecessaryAndMovePlayers);
            _signalBus.Subscribe<PlayerStopMovingSignal>(PlayerStopMoving);
            _signalBus.Subscribe<BlockStopMovingSignal>(BlockStopMoving);
            _signalBus.Subscribe<WinGameSignal>(PlayerWinGame);
            _signalBus.Subscribe<TutorialCompletedSignal>(PlayerWinGame);


            _winGame = false;
            _movementsModel.Initialize();
        }

        private void PlayerWinGame(WinGameSignal obj)
        {
            _winGame = true;
        }

        private void BlockStopMoving(BlockStopMovingSignal obj)
        {
            CheckIfPlayersCanMoveAgain();
        }

        private void PlayerStopMoving(PlayerStopMovingSignal obj)
        {
            AddPlayerToList(obj.Player);
            CheckIfPlayersCanMoveAgain();
        }

        private void CheckIfPlayersCanMoveAgain()
        {
            if (_playersStopped.Count == NUM_PLAYERS && !_blockMovementModel.IsBlockMoving() && !_winGame)
            {
                // Debug.Log("Player Can Move Now");
                _playersStopped.Clear();
                _movementsModel.EnableMovements();
            }
        }

        private void AddPlayerToList(PlayerBase objPlayer)
        {
            if (_playersStopped.Contains(objPlayer))
            {
                return;
            }

            _playersStopped.Add(objPlayer);
        }

        private void PlayerTwoInitialized(PlayerTwoInitializedSignal obj)
        {
            _playersPositionsModel.PositionPlayerTwo = obj.InitialPosition;
        }

        private void PlayerOneInitialized(PlayerOneInitializedSignal obj)
        {
            _playersPositionsModel.PositionPlayerOne = obj.InitialPosition;
        }

        private void MoveToNewCubeSignal(MoveToNewCubeSignal obj)
        {
            Vector3 cubePositionToMove = obj.CubePosition;
            if (!_movementsModel.CanMove() || PlayerIsInSamePosition(cubePositionToMove))
            {
                return;
            }

            // Debug.Log(cubePositionToMove);

            if (IsMoveInVertical(cubePositionToMove))
            {
                if (CanMoveForward(cubePositionToMove))
                {
                    int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.x);
                    int zCube = Mathf.RoundToInt(cubePositionToMove.x);
                    int numMoves = Mathf.Abs(zPlayer - zCube);

                    //TODO CHECK THE REMAINING MOVES
                    
                    _signalBus.Fire(new MoveForwardSignal {NumMoves = numMoves});
                    return;
                }

                if (CanMoveBackward(cubePositionToMove))
                {
                    int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.x);
                    int zCube = Mathf.RoundToInt(cubePositionToMove.x);
                    int numMoves = Mathf.Abs(zPlayer - zCube);

                    _signalBus.Fire(new MoveBackwardSignal {NumMoves = numMoves});
                    return;
                }
            }

            if (IsMoveInHorizontal(cubePositionToMove))
            {
                if (CanMoveLeft(cubePositionToMove))
                {
                    int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.z);
                    int zCube = Mathf.RoundToInt(cubePositionToMove.z);
                    int numMoves = Mathf.Abs(zPlayer - zCube);

                    _signalBus.Fire(new MoveLeftSignal {NumMoves = numMoves});
                    return;
                }

                if (CanMoveRight(cubePositionToMove))
                {
                    int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.z);
                    int zCube = Mathf.RoundToInt(cubePositionToMove.z);
                    int numMoves = Mathf.Abs(zPlayer - zCube);

                    _signalBus.Fire(new MoveRightSignal {NumMoves = numMoves});
                    return;
                }
            }
        }

        private bool PlayerIsInSamePosition(Vector3 cubePositionToMove)
        {
            return cubePositionToMove == _playersPositionsModel.PositionPlayerOne;
        }

        #region Check type of movement

        private bool CanMoveRight(Vector3 cubePositionToMove)
        {
            int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.z);
            int zCube = Mathf.RoundToInt(cubePositionToMove.z);
            return zCube < zPlayer;
        }

        private bool CanMoveLeft(Vector3 cubePositionToMove)
        {
            int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.z);
            int zCube = Mathf.RoundToInt(cubePositionToMove.z);

            return zCube > zPlayer;
        }

        private bool CanMoveBackward(Vector3 cubePositionToMove)
        {
            int xPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.x);
            int xCube = Mathf.RoundToInt(cubePositionToMove.x);

            return xCube < xPlayer;
        }

        private bool CanMoveForward(Vector3 cubePositionToMove)
        {
            int xPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.x);
            int xCube = Mathf.RoundToInt(cubePositionToMove.x);

            return xCube > xPlayer;
        }

        private bool IsMoveInHorizontal(Vector3 cubePositionToMove)
        {
            int xPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.x);
            int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.z);
            int zCube = Mathf.RoundToInt(cubePositionToMove.z);
            int xCube = Mathf.RoundToInt(cubePositionToMove.x);

            return (xCube).Equals(xPlayer) && !(zCube).Equals(zPlayer);
        }

        private bool IsMoveInVertical(Vector3 cubePositionToMove)
        {
            int xPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.x);
            int zPlayer = Mathf.RoundToInt(_playersPositionsModel.PositionPlayerOne.z);
            int zCube = Mathf.RoundToInt(cubePositionToMove.z);
            int xCube = Mathf.RoundToInt(cubePositionToMove.x);

            return (zCube).Equals(zPlayer) && !(xCube).Equals(xPlayer);
        }

        #endregion

        private void DisablePlayerMovement(DisablePlayerMovementSignal obj)
        {
            _movementsModel.DisableMovements();
        }

        private void EnablePlayerMovement(EndTutorialExplicationSignal signal)
        {
            _movementsModel.EnableMovements();
        }


        private void RestartGame(RestartGameSignal obj)
        {
            _movementsModel.Initialize();
        }

        #region Calculate Movements To do

        private void InitMoveForward(MoveForwardSignal signal)
        {
            if (_movementsModel.CanMove())
            {
                _signalBus.Fire(new CalculateNextMovePlayerOneSignal()
                    {TypeMovement = TypeMovement.FORWARD, NumMoves = signal.NumMoves});
            }
        }

        private void InitMoveBackward(MoveBackwardSignal signal)
        {
            if (_movementsModel.CanMove())
            {
                _signalBus.Fire(new CalculateNextMovePlayerOneSignal()
                    {TypeMovement = TypeMovement.BACK, NumMoves = signal.NumMoves});
            }
        }

        private void InitMoveRight(MoveRightSignal signal)
        {
            if (_movementsModel.CanMove())
            {
                _signalBus.Fire(new CalculateNextMovePlayerOneSignal()
                    {TypeMovement = TypeMovement.RIGHT, NumMoves = signal.NumMoves});
            }
        }

        private void InitMoveLeft(MoveLeftSignal signal)
        {
            if (_movementsModel.CanMove())
            {
                _signalBus.Fire(new CalculateNextMovePlayerOneSignal()
                    {TypeMovement = TypeMovement.LEFT, NumMoves = signal.NumMoves});
            }
        }

        private void UpdateMoveIfNecessaryAndMovePlayers(RetrieveMovementsToDoBasedOnPlayerTwoSignal signal)
        {
            //TODO Sumatorio movimientos
            _movementsModel.UpdateCounter(signal.NumMovesPlayerOne);

            _signalBus.Fire(new LoadMovementsSignal()
            {
                Movements = signal.NumMovesPlayerOne
            });
            

            switch (signal.InfoMovement)
            {
                case TypeMovement.FORWARD:

                    _signalBus.Fire(new PlayerOneMoveSignal()
                        {TypeMovement = TypeMovement.BACK});
                    _signalBus.Fire(new PlayerTwoMoveSignal()
                        {TypeMovement = TypeMovement.FORWARD});
                    _signalBus.Fire<DisablePlayerMovementSignal>();
                    break;
                case TypeMovement.RIGHT:
                    _signalBus.Fire(new PlayerOneMoveSignal()
                        {TypeMovement = TypeMovement.LEFT});
                    _signalBus.Fire(new PlayerTwoMoveSignal()
                        {TypeMovement = TypeMovement.RIGHT});
                    _signalBus.Fire<DisablePlayerMovementSignal>();
                    break;
                case TypeMovement.LEFT:
                    _signalBus.Fire(new PlayerOneMoveSignal()
                        {TypeMovement = TypeMovement.RIGHT});
                    _signalBus.Fire(new PlayerTwoMoveSignal()
                        {TypeMovement = TypeMovement.LEFT});
                    _signalBus.Fire<DisablePlayerMovementSignal>();
                    break;
                case TypeMovement.BACK:
                    _signalBus.Fire(new PlayerOneMoveSignal()
                        {TypeMovement = TypeMovement.FORWARD});
                    _signalBus.Fire(new PlayerTwoMoveSignal()
                        {TypeMovement = TypeMovement.BACK});
                    _signalBus.Fire<DisablePlayerMovementSignal>();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}