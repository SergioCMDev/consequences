﻿using Code.Infra.Interfaces;
using Code.Infra.Signals.Actions;
using Code.Managers.Coroutines;
using Code.Utilities;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Presenters
{
    public class BlocksManager : IInitializable
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private IBlocksPositionModel _blocksPositionModel;
        [Inject] private IBlockStatusMovementModel _blockMovementModel;

        private float speedMovementAttachedObject = 1.0f;

        public void Initialize()
        {
            _signalBus.Subscribe<MoveBlockDownSignal>(MoveBlockDown);
            _signalBus.Subscribe<AddBockPositionSignal>(AddBlockPosition);
            _signalBus.Subscribe<MoveBlockDownSignal>(RemoveBlockPosition);
            _signalBus.Subscribe<RestartGameSignal>(ClearBlocks);
            _blocksPositionModel.CleanBlocks();
        }

        private void ClearBlocks(RestartGameSignal obj)
        {
            _blocksPositionModel.CleanBlocks();
        }

        private void RemoveBlockPosition(MoveBlockDownSignal obj)
        {
            _blocksPositionModel.RemoveBlock(obj.Block.transform.position);
        }

        private void AddBlockPosition(AddBockPositionSignal obj)
        {
            _blocksPositionModel.AddBlock(obj.BlockPositionToAdd);
        }

        private void MoveBlockDown(MoveBlockDownSignal signal)
        {
            Transform transformBlockAttached = signal.Block.transform;
            Vector3 attachedObjectOriginPos = transformBlockAttached.position;

            Vector3 destinationAttachedObject = attachedObjectOriginPos - Vector3.up * 1.1f;

            MoveCoroutine moveCoroutine = new MoveCoroutine(transformBlockAttached, attachedObjectOriginPos,
                destinationAttachedObject, speedMovementAttachedObject);
            moveCoroutine.OnCoroutineEnd += OnBlockEndMovement;
            _blockMovementModel.BlockStartMoving();
            CoroutineManager.Instance.InitCoroutine(moveCoroutine.Start());
        }

        private void OnBlockEndMovement(MoveCoroutine moveCoroutine)
        {
            moveCoroutine.OnCoroutineEnd -= OnBlockEndMovement;
            moveCoroutine.ObjectTransform.gameObject.SetActive(false);
            _blockMovementModel.BlockStopMoving();
            _signalBus.Fire(new BlockStopMovingSignal());
        }
    }
}