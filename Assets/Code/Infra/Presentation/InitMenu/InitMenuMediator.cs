﻿using System;
using System.Collections.Generic;
using Code.Infra.Dto;
using Code.Infra.Presentation.Views;
using Code.Infra.Signals.Sounds;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Code.Infra.Presentation.InitMenu
{
    public class InitMenuMediator : MonoBehaviour
    {
        [SerializeField] private StartMenuView _startMenu;
        [SerializeField] private SelectionLevelsMenu _selectionLevelsMenu;

        [SerializeField] private TextMeshProUGUI textVersion;
        [Inject] private SignalBus _signalBus;
        public event Action OnPlayTutorialButtonPressedEvent;
        public event Action OnContinueCurrentLevel;
        public event Action OnShowSelectionLevels;

        public event Action<string> OnLevelSelected;


        void Start()
        {
            _signalBus.Fire(new PlayMenuThemeSoundSignal() {Play = true});

            Assert.IsNotNull(_startMenu, "Start Menu has to be added");
            Assert.IsNotNull(_selectionLevelsMenu, "Selection Level Menu has to be added");

            Assert.IsNotNull(textVersion, "Text version Must be added");
            textVersion.text = string.Format("V{0}", Application.version);

            ShowInitMenu();

            _startMenu.OnTutorialLoad += PlayTutorial;
            _startMenu.OnLoadCurrentLevel += LoadCurrentLevel;
            _startMenu.OnSelectionLevelButtonPressed += OpenSelectionLevel;
            _selectionLevelsMenu.OnLevelSelected += LevelSelected;
            _selectionLevelsMenu.OnButtonBackPressed += ShowInitMenu;
        }

        private void ShowInitMenu()
        {
            _startMenu.gameObject.SetActive(true);
            _selectionLevelsMenu.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _startMenu.OnTutorialLoad -= PlayTutorial;
            _startMenu.OnLoadCurrentLevel -= LoadCurrentLevel;
            _startMenu.OnSelectionLevelButtonPressed -= OpenSelectionLevel;
            _selectionLevelsMenu.OnLevelSelected -= LevelSelected;
            _selectionLevelsMenu.OnButtonBackPressed -= ShowInitMenu;
        }

        private void OpenSelectionLevel()
        {
            OnShowSelectionLevels.Invoke();
            _signalBus.Fire(new PlayClickSoundSignal());

        }

        private void LevelSelected(string sceneName)
        {
            _signalBus.Fire(new PlayMenuThemeSoundSignal()
            {
                Play = false
            });
            _signalBus.Fire(new PlayStartLevelSoundSignal());

            OnLevelSelected.Invoke(sceneName);
        }

        private void PlayTutorial()
        {
            _signalBus.Fire(new PlayClickSoundSignal());
            OnPlayTutorialButtonPressedEvent.Invoke();
        }

        public void ChangeToLevelSelection(List<LevelDto> levelsData)
        {
            _startMenu.gameObject.SetActive(false);
            _selectionLevelsMenu.gameObject.SetActive(true);
            _selectionLevelsMenu.SetLevelData(levelsData);
        }


        private void LoadCurrentLevel()
        {
            _signalBus.Fire(new PlayClickSoundSignal());

            OnContinueCurrentLevel.Invoke();
        }

        public void HidePlayButton()
        {
            _startMenu.ShowOthersButtons();
        }

        public void ShowPlayButton()
        {
            _startMenu.ShowPlayButton();
        }
    }
}