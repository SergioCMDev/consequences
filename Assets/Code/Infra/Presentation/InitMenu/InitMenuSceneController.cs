﻿using System.Collections.Generic;
using Code.App.Controllers;
using Code.Domain.Interfaces;
using Code.Infra.Dto;
using Code.Infra.Interfaces;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using Zenject;

namespace Code.Infra.Presentation.InitMenu
{
    public class InitMenuSceneController : IInitializable
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private InitMenuMediator _initMenuMediator;
        [Inject] private ITutorialModel _tutorialModel;
        [Inject] private ILevelModel _levelModel;
        [Inject] private AudioController _audioController;

        [Inject] private ILevelsDataManager _levelsDataManager;

        public void Initialize()
        {
            _initMenuMediator.OnPlayTutorialButtonPressedEvent += PlayTutorialButtonPressed;
            _initMenuMediator.OnContinueCurrentLevel += ContinueCurrentLevel;
            _initMenuMediator.OnShowSelectionLevels += ShowSelectionLevels;
            _initMenuMediator.OnLevelSelected += LevelSelected;

            if (_tutorialModel.IsTutorialDone())
            {
                _initMenuMediator.HidePlayButton();
            }
            else
            {
                _initMenuMediator.ShowPlayButton();
            }
        }

        private void ShowSelectionLevels()
        {
            List<LevelDto> levelsData = GetLevelsData();
            _initMenuMediator.ChangeToLevelSelection(levelsData);
        }

        private void ContinueCurrentLevel()
        {
            string sceneName = GetUnlockedLastLevel().SceneName;
            LevelSelected(sceneName);
        }


        private void LevelSelected(string sceneName)
        {
            _signalBus.Fire(new PlayMenuThemeSoundSignal() {Play = false});

            _signalBus.Fire(new LoadLevelSelectedSignal() {SceneName = sceneName});
            _signalBus.Fire(new PlayStartLevelSoundSignal());
            _levelModel.UpdateMaxMovements(sceneName);
        }

        private void PlayTutorialButtonPressed()
        {
            _signalBus.Fire(new PlayMenuThemeSoundSignal() {Play = false});

            _levelModel.UpdateMaxMovements("Tutorial");

            _signalBus.Fire(new LoadTutorialSignal());
        }

        private List<LevelDto> GetLevelsData()
        {
            return _levelsDataManager.GetAllLevelsData();
        }

        private LevelDto GetUnlockedLastLevel()
        {
            return _levelsDataManager.GetUnlockedLastLevel();
        }
    }
}