﻿using Code.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Infra.Presentation.InitMenu
{
    public class UnlockedLevelView : MonoBehaviour
    {
        [SerializeField] private GameObject _starsPlaceHolder;

        [SerializeField] private TextMeshProUGUI _levelId;

        [SerializeField] private Image _background;

        public void SetStars(int levelDtoStarsAcheived, int starsMaxLevel)
        {
            GameObject starFilledPrefab = Resources.Load<GameObject>(Constants.Prefabs.STAR_FILLED_PATH);
            GameObject starEmptyPrefab = Resources.Load<GameObject>(Constants.Prefabs.STAR_EMPTY_PATH);
            for (int i = 0; i < levelDtoStarsAcheived; i++)
            {
                Instantiate(starFilledPrefab, _starsPlaceHolder.transform, false);
            }

            for (int i = 0; i < starsMaxLevel - levelDtoStarsAcheived; i++)
            {
                Instantiate(starEmptyPrefab, _starsPlaceHolder.transform, false);
            }
        }

        public void SetIdLevel(int id)
        {
            _levelId.SetText(id.ToString());
        }
    }
}