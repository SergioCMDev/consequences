﻿using Code.Infra.Dto;
using Code.Infra.Models;
using ScriptableObjects.Templates;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Infra.Presentation.InitMenu
{
    public class LevelInfoView : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private UnlockedLevelView _unlockPlaceHolder;
        [SerializeField] private GameObject _lockPlaceHolder;
        public event System.Action<int> OnLevelClicked = delegate { };

        private int _id;
        private bool _unlocked;

        private void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }


        public void SetInfo(LevelDto levelDto)
        {
            _unlocked = levelDto.Unlocked;
            _id = levelDto.Id;
            if (_unlocked)
            {
                _unlockPlaceHolder.SetStars(levelDto.StarsAcheived, levelDto.TotalStarsAtLevel);
                _unlockPlaceHolder.SetIdLevel(levelDto.Id);
                DisableLock();
                return;
            }

            EnableLock();
        }

        private void EnableLock()
        {
            _lockPlaceHolder.gameObject.SetActive(true);
            _unlockPlaceHolder.gameObject.SetActive(false);
        }

        private void DisableLock()
        {
            _lockPlaceHolder.gameObject.SetActive(false);
            _unlockPlaceHolder.gameObject.SetActive(true);
            
        }

        public void OnButtonClicked()
        {
            if (!_unlocked)
            {
                Debug.Log("LOCK " + _id);
                return;
            }

            OnLevelClicked(_id);
        }
    }
}