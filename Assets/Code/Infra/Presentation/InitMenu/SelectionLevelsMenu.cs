﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Infra.Dto;
using Code.Infra.Models;
using Code.Infra.Signals.Sounds;
using Code.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.InitMenu
{
    public class SelectionLevelsMenu : MonoBehaviour
    {
        public event Action<string> OnLevelSelected;
        public event Action OnButtonBackPressed;
        [Inject] private SignalBus _signalBus;

        //TODO IN CASE WE WANT SCROLL SELECTORS
        // [SerializeField] private ListPositionCtrl _listPositionCtrl;
        private void Awake()
        {
            // _listPositionCtrl.OnDragEndEvent += DragEventEnd;
            // _listPositionCtrl.OnLevelClickedEvent += LevelClicked;
        }
        // private void DragEventEnd(int idLevel)
        // {
        //     LevelDto selectedLevel = _levelsData.SingleOrDefault(x => x.Id == idLevel);
        //     if (selectedLevel != null)
        //     {
        //         _starsMoves.SetText($"Stars {selectedLevel.StarsAcheived.ToString()} / {selectedLevel.TotalStarsAtLevel}");
        //         _movesText.SetText($"Best Moves {selectedLevel.Movements.ToString()}");
        //         _levelTitle.SetText(selectedLevel.LevelName);
        //     }
        // }


        [SerializeField] private TextMeshProUGUI _movesText;
        [SerializeField] private TextMeshProUGUI _starsMoves;
        [SerializeField] private TextMeshProUGUI _levelTitle;
        [SerializeField] private GameObject _levelGridLayoutView;
        [SerializeField] private Button _buttonBack;

        private List<LevelDto> _levelsData;

        private void Start()
        {
            _buttonBack.onClick.AddListener(ButtonClickPressed);
        }

        private void ButtonClickPressed()
        {
            _signalBus.Fire(new PlayClickSoundSignal());

            OnButtonBackPressed.Invoke();
        }

        public void SetLevelData(List<LevelDto> levelsData)
        {
            if (_levelsData != null)
            {
                return;
            }
            _levelsData = levelsData;
            GameObject levelBoxPrefab = Resources.Load<GameObject>(Constants.Prefabs.LEVEL_BOX);
            foreach (LevelDto level in _levelsData)
            {
                GameObject levelBoxInstance = Instantiate(levelBoxPrefab, _levelGridLayoutView.transform, false);
                LevelInfoView levelInfoView = levelBoxInstance.GetComponent<LevelInfoView>();
                levelInfoView.SetInfo(level);
                levelInfoView.OnLevelClicked += LevelClicked;
            }
        }

        public void LevelClicked(int idLevel)
        {
            LevelDto selectedLevel = _levelsData.Single(x => x.Id == idLevel);
            OnLevelSelected.Invoke(selectedLevel.SceneName);
        }
    }
}