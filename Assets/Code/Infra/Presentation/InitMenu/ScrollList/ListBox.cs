﻿// /* The basic component of the scrolling list.
//  * Control the position and update the content of the list element.
//  */
// using UnityEngine;
//
// public class ListBox : MonoBehaviour
// {
//     
//     [HideInInspector] public ListBox LastListBox;
//     [HideInInspector] public ListBox NextListBox;
//
//     private int _creationId;
//     private int _idLevel;
//
//     private ListPositionCtrl _positionCtrl;
//
//     // private BaseListBank _listBank;
//     public int PositionOnCreation
//     {
//         get { return _creationId; }
//         private set { _creationId = value; }
//     }
//
//
//     public int CountBoxes { get; set; }
//
//     public int IdOfLevel
//     {
//         get { return _idLevel; }
//         private set { _idLevel = value; }
//     }
//
//     /* ====== Position variables ====== */
//     // Position caculated here is in the local space of the list
//     private Vector2 _maxCurvePos; // The maximum outer position
//     private Vector2 _unitPos; // The distance between boxes
//     private Vector2 _lowerBoundPos; // The left/down-most position of the box
//
//     private Vector2 _upperBoundPos; // The right/up-most position of the box
//
//     // _changeSide(Lower/Upper)BoundPos is the boundary for checking that
//     // whether to move the box to the other end or not
//     private Vector2 _changeSideLowerBoundPos;
//     private Vector2 _changeSideUpperBoundPos;
//     private float _cosValueAdjust;
//
//     private Vector3 _slidingDistance; // The sliding distance for each frame
//     private Vector3 _slidingDistanceLeft;
//
//     private Vector3 _initialLocalScale;
//
//     /* Notice: ListBox will initialize its variables from ListPositionCtrl.
//      * Make sure that the execution order of script ListPositionCtrl is prior to
//      * ListBox.
//      */
//     void Start()
//     {
//         _positionCtrl = transform.GetComponentInParent<ListPositionCtrl>();
//
//         _maxCurvePos = _positionCtrl.canvasMaxPos_L * _positionCtrl.listCurvature;
//         _unitPos = _positionCtrl.unitPos_L;
//         _lowerBoundPos = _positionCtrl.lowerBoundPos_L;
//         _upperBoundPos = _positionCtrl.upperBoundPos_L;
//         _changeSideLowerBoundPos = _lowerBoundPos + _unitPos * 0.3f;
//         _changeSideUpperBoundPos = _upperBoundPos - _unitPos * 0.3f;
//         _cosValueAdjust = _positionCtrl.positionAdjust;
//
//         _initialLocalScale = transform.localScale;
//
//         InitialPosition();
//     }
//
//     void InitialPosition()
//     {
//         // If there are even number of ListBoxes, adjust the initial position by an half unitPos.
//         if ((CountBoxes & 0x1) == 0)
//         {
//             switch (_positionCtrl.direction)
//             {
//                 case ListPositionCtrl.Direction.Vertical:
//                     transform.localPosition = new Vector3(0.0f,
//                         _unitPos.y * (PositionOnCreation * -1 + CountBoxes / 2) - _unitPos.y / 2, 0.0f);
//                     UpdateXPosition();
//                     break;
//                 case ListPositionCtrl.Direction.Horizontal:
//                     transform.localPosition = new Vector3(
//                         _unitPos.x * (PositionOnCreation - CountBoxes / 2) - _unitPos.x / 2,
//                         0.0f, 0.0f);
//                     UpdateYPosition();
//                     break;
//             }
//         }
//         else
//         {
//             switch (_positionCtrl.direction)
//             {
//                 case ListPositionCtrl.Direction.Vertical:
//                     transform.localPosition = new Vector3(0.0f, _unitPos.y * (PositionOnCreation * -1 + CountBoxes / 2),
//                         0.0f);
//                     UpdateXPosition();
//                     break;
//                 case ListPositionCtrl.Direction.Horizontal:
//                     transform.localPosition = new Vector3(
//                         _unitPos.x * (PositionOnCreation - CountBoxes / 2), 0.0f, 0.0f);
//                     UpdateYPosition();
//                     break;
//             }
//         }
//     }
//
//     /* Update the local position of ListBox accroding to the delta position at each frame.
//      * Note that the deltaPosition must be in local space.
//      */
//     public void UpdatePosition(Vector3 deltaPosition_L)
//     {
//         switch (_positionCtrl.direction)
//         {
//             case ListPositionCtrl.Direction.Vertical:
//                 transform.localPosition += new Vector3(0.0f, deltaPosition_L.y, 0.0f);
//                 CheckBoundaryY();
//                 UpdateXPosition();
//                 break;
//             case ListPositionCtrl.Direction.Horizontal:
//                 transform.localPosition += new Vector3(deltaPosition_L.x, 0.0f, 0.0f);
//                 CheckBoundaryX();
//                 UpdateYPosition();
//                 break;
//         }
//     }
//
//     /* Calculate the x position accroding to the y position.
//      */
//     void UpdateXPosition()
//     {
//         // Formula: x = maxCurvePos_x * (cos(r) + cosValueAdjust),
//         // where r = (y / upper_y) * pi / 2, then r is in range [- pi / 2, pi / 2],
//         // and corresponding cosine value is from 0 to 1 to 0.
//         transform.localPosition = new Vector3(
//             _maxCurvePos.x * (_cosValueAdjust +
//                               Mathf.Cos(transform.localPosition.y / _upperBoundPos.y * Mathf.PI / 2.0f)),
//             transform.localPosition.y, transform.localPosition.z);
//         UpdateSize(_upperBoundPos.y, transform.localPosition.y);
//     }
//
//     /* Calculate the y position accroding to the x position.
//      */
//     void UpdateYPosition()
//     {
//         transform.localPosition = new Vector3(
//             transform.localPosition.x,
//             _maxCurvePos.y * (_cosValueAdjust +
//                               Mathf.Cos(transform.localPosition.x / _upperBoundPos.x * Mathf.PI / 2.0f)),
//             transform.localPosition.z);
//         UpdateSize(_upperBoundPos.x, transform.localPosition.x);
//     }
//
//     /* Check if the ListBox is beyond the checking boundary or not
//      * If it does, move the ListBox to the other end of the list
//      * and update the content.
//      */
//     void CheckBoundaryY()
//     {
//         float beyondPosY_L = 0.0f;
//
//         if (transform.localPosition.y < _changeSideLowerBoundPos.y)
//         {
//             beyondPosY_L = transform.localPosition.y - _lowerBoundPos.y;
//             transform.localPosition = new Vector3(
//                 transform.localPosition.x,
//                 _upperBoundPos.y - _unitPos.y + beyondPosY_L,
//                 transform.localPosition.z);
//             // UpdateToLastContent();
//         }
//         else if (transform.localPosition.y > _changeSideUpperBoundPos.y)
//         {
//             beyondPosY_L = transform.localPosition.y - _upperBoundPos.y;
//             transform.localPosition = new Vector3(
//                 transform.localPosition.x,
//                 _lowerBoundPos.y + _unitPos.y + beyondPosY_L,
//                 transform.localPosition.z);
//             // UpdateToNextContent();
//         }
//     }
//
//     void CheckBoundaryX()
//     {
//         float beyondPosX_L = 0.0f;
//
//         if (transform.localPosition.x < _changeSideLowerBoundPos.x)
//         {
//             beyondPosX_L = transform.localPosition.x - _lowerBoundPos.x;
//             transform.localPosition = new Vector3(
//                 _upperBoundPos.x - _unitPos.x + beyondPosX_L,
//                 transform.localPosition.y,
//                 transform.localPosition.z);
//             // UpdateToNextContent();
//         }
//         else if (transform.localPosition.x > _changeSideUpperBoundPos.x)
//         {
//             beyondPosX_L = transform.localPosition.x - _upperBoundPos.x;
//             transform.localPosition = new Vector3(
//                 _lowerBoundPos.x + _unitPos.x + beyondPosX_L,
//                 transform.localPosition.y,
//                 transform.localPosition.z);
//             // UpdateToLastContent();
//         }
//     }
//
//     /* Scale the listBox accroding to its position
//      *
//      * @param smallest_at The position at where the smallest listBox will be
//      * @param target_value The position of the target listBox
//      */
//     void UpdateSize(float smallest_at, float target_value)
//     {
//         // The scale of the box at the either end is initialLocalScale.
//         // The scale of the box at the center is initialLocalScale * (1 + centerBoxScaleRatio).
//         transform.localScale = _initialLocalScale *
//                                (1.0f + _positionCtrl.centerBoxScaleRatio *
//                                    Mathf.InverseLerp(smallest_at, 0.0f, Mathf.Abs(target_value)));
//     }
//
//
//     public void SetPositionId(int id)
//     {
//         _creationId = id;
//     }
//
//     public void SetLevelId(int id)
//     {
//         _idLevel = id;
//     }
// }