﻿using Code.Domain.Interfaces;
using Code.Enums;
using Code.Infra.Interfaces;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using Code.Infra.Signals.UpdateViews;
using UnityEngine;
using Zenject;
using SceneManager = Code.Domain.Scenes.SceneManager;

namespace Code.Infra.Presentation.Views
{
    public class CanvasController : IInitializable
    {
        [Inject] private MovementsCounterView _movementsCounterView;
        [Inject] private AudioStateView _audioStateView;
        [Inject] private ILevelModel _levelModel;
        [Inject] private IMovementsModel _movementsModel;
        [Inject] private IAudioModel _AudioModel;

        [Inject] private SignalBus _signalBus;
        [Inject] private DiContainer _diContainer;
        [Inject] readonly WinMenuView.Factory _winMenufactory;
        [Inject] readonly DefeatMenuView.Factory _defeatMenufactory;
        [Inject] private SceneManager _sceneManager;

        public void Initialize()
        {
            _signalBus.Subscribe<WinGameSignal>(ShowVictoryMenu);
            _signalBus.Subscribe<LostGameSignal>(ShowDefeatMenu);
            _signalBus.Subscribe<LoadMovementsSignal>(SetMovements);
            _signalBus.Subscribe<AudioStateChangedSignal>(ChangeAudioState);
            _signalBus.Fire(new PlayGameThemeSoundSignal() {Play = true});

            _levelModel.UpdateMaxMovements(_sceneManager.GetActualScene().name);

            _movementsModel.RemainingMovements = _levelModel.LevelDto.MaxMovements;
            _movementsCounterView.UpdateCounter(_movementsModel.RemainingMovements);
        }

        private void SetMovements(LoadMovementsSignal obj)
        {
            _movementsModel.RemainingMovements -= obj.Movements;
            _movementsCounterView.UpdateCounter(_movementsModel.RemainingMovements);
        }

        private void ShowDefeatMenu(LostGameSignal obj)
        {
            _signalBus.Fire(new PlayGameThemeSoundSignal() {Play = false});
            DefeatMenuView defeatMenuView = _defeatMenufactory.Create();
            Canvas canvas = _diContainer.Resolve<Canvas>();
            defeatMenuView.transform.SetParent(canvas.transform, false);
        }

        private void ShowVictoryMenu(WinGameSignal signal)
        {
            _signalBus.Fire(new PlayGameThemeSoundSignal() {Play = false});
            bool lastScene = _sceneManager.IsLastScene();
            WinMenuView winMenuView = _winMenufactory.Create(signal.Movements, signal.Stars, lastScene);
            Canvas canvas = _diContainer.Resolve<Canvas>();
            winMenuView.transform.SetParent(canvas.transform, false);
        }

        private void ChangeAudioState()
        {
            _AudioModel.ChangeAudioState();
            if (_AudioModel.GetStateAudio() == AudioState.Enabled)
            {
                _audioStateView.SetEnableAudioImage();
                return;
            }

            _audioStateView.SetDisableAudioImage();
        }
    }
}