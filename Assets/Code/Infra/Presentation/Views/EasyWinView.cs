﻿using Code.Infra.Signals.Actions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class EasyWinView : MonoBehaviour
    {
        [SerializeField] private Button _buttonToWin;

        [SerializeField] private GameObject _easyWinPlaceholder;
        [Inject] private SignalBus _signalBus;

        void Start()
        {
#if !UNITY_EDITOR
        _easyWinPlaceholder.SetActive(false);
#endif
            _buttonToWin.onClick.AddListener(EasWin);
        }

        private void OnDestroy()
        {
            _buttonToWin.onClick.RemoveListener(EasWin);

        }

        private void EasWin()
        {
            _signalBus.Fire(new WinGameSignal()
            {
                SceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name,
                Movements = 0,
                Stars = 0
            });
        }
    }
}