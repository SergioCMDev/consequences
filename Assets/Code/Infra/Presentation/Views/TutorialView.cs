﻿using System;
using Code.Infra.Signals.Sounds;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class TutorialView : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;

        [SerializeField] private GameObject _initTutorialWindow,
            _introKatWindow,
            _introNekoWindow,
            _introGoalsWindow,
            _activatorsInfoWindow,
            _movementsInfoWindow,
            _startGameWindow,
            _victoryGameWindow;

        [SerializeField] private TextMeshProUGUI _textKatWindow,
            _textNekoWindow,
            _textGoalsWindow,
            _textStartGameWindow,
            _textGoalNekoWindow,
            _textMovementsWindow,
            _textStartTutorialWindow,
            _textActivatorsWindow;

        [SerializeField] private Button _nextPanelInitTutorialButton,
            _nextPanelKatInfoButton,
            _nextPanelNekoInfoButton,
            _nextPanelMovementsButton,
            _nextPanelActivatorsButton,
            _nextPanelGoalsButton,
            _startGameButton;

        public event Action OnButtonNextInitTutorialClicked = delegate { };
        public event Action OnButtonNextKatInfoClicked = delegate { };
        public event Action OnButtonNextNekoInfoClicked = delegate { };
        public event Action OnButtonNextMovementsInfoClicked = delegate { };
        public event Action OnButtonNextGoalsInfoClicked = delegate { };
        public event Action OnButtonNextActivatorsInfoClicked = delegate { };
        public event Action OnButtonStartGameInfoClicked = delegate { };

        private void Start()
        {
        
            //TODO Obtener de JSON
            _textStartTutorialWindow.SetText(
                "Welcome to the tutorial. Here you will learn how to move Kat and Neko to pass the levels.");
            _textKatWindow.SetText("This is the side of Kat, the player you will control.");
            _textNekoWindow.SetText("This is the side of Neko.");
            _textGoalsWindow.SetText(
                "Your goal is to reach both Goals, the fixed squares with red background, with Kat and Neko at the same time.");
            _textMovementsWindow.SetText(
                "You can move just pushing the block you want to go, you can move in 4 ways, UP, DOWN, LEFT and RIGHT, but you can move several blocks in a row, like a queen in chess");
            _textActivatorsWindow.SetText(
                "You can move down any block that is blocking a path if any character press the right activator, the buttons with a transparent colour around them, of their respectively colour.");
            _textStartGameWindow.SetText("Now, you can start this level and pass it :)");

            _nextPanelInitTutorialButton.onClick.AddListener(ButtonNextInitTutorialClicked);
            _nextPanelKatInfoButton.onClick.AddListener(ButtonNextKatInfoClicked);
            _nextPanelNekoInfoButton.onClick.AddListener(ButtonNextNekoInfoClicked);
            _nextPanelMovementsButton.onClick.AddListener(ButtonNextMovementsClicked);
            _nextPanelActivatorsButton.onClick.AddListener(ButtonNextActivatorsClicked);
            _nextPanelGoalsButton.onClick.AddListener(ButtonNextGoalsClicked);
            _startGameButton.onClick.AddListener(ButtonStartGameClicked);

            _initTutorialWindow.SetActive(true);
        }

        private void ButtonNextInitTutorialClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonNextInitTutorialClicked.Invoke();
        }

        private void ButtonNextKatInfoClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonNextKatInfoClicked.Invoke();
        }

        private void ButtonNextNekoInfoClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonNextNekoInfoClicked.Invoke();
        }

        private void ButtonNextMovementsClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonNextMovementsInfoClicked.Invoke();
        }

        private void ButtonNextActivatorsClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonNextActivatorsInfoClicked.Invoke();
        }

        private void ButtonNextGoalsClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonNextGoalsInfoClicked.Invoke();
        }

        private void ButtonStartGameClicked()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            OnButtonStartGameInfoClicked.Invoke();
        }


        public void ChangeToIntroKatWindow()
        {
            _initTutorialWindow.SetActive(false);
            _introKatWindow.SetActive(true);
        }

        public void ChangeToIntroNekoWindow()
        {
            _introKatWindow.SetActive(false);
            _introNekoWindow.SetActive(true);
        }

        public void ChangeToMovementsWindow()
        {
            _introNekoWindow.SetActive(false);
            _movementsInfoWindow.SetActive(true);
        }

        public void ChangeToActivatorsWindow()
        {
            _movementsInfoWindow.SetActive(false);
            _activatorsInfoWindow.SetActive(true);
        }

        public void ChangeToGoalsWindow()
        {
            _activatorsInfoWindow.SetActive(false);
            _introGoalsWindow.SetActive(true);
        }

        public void ChangeToStartPlayWindow()
        {
            _introGoalsWindow.SetActive(false);
            _startGameWindow.SetActive(true);
        }

        public void CloseAllWindows()
        {
            _startGameWindow.SetActive(false);
        }

        public void ShowVictoryWindow()
        {
            _victoryGameWindow.SetActive(true);
        }
    }
}