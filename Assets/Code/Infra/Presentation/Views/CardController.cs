﻿using System;
using Code.Infra.Models;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Code.Infra.Presentation.Views
{
    public class CardController : MonoBehaviour {
        [SerializeField]
        private MovementInfo movement;
        [SerializeField]
        private Sprite sprite;

        private bool canBeUsed;
        public MovementInfo Movement { get => movement; set => movement = value; }
        public bool CanBeUsed { get => canBeUsed; set => canBeUsed = value; }

        public event Action<CardController> OnCardUsed = delegate { };
        // Start is called before the first frame update
        void Start()
        {
            GetComponent<Image>().sprite = sprite;
            CanBeUsed = true;
        }
        public void OnMouseDown()
        {
            if (CanBeUsed)
            {

                //Debug.Log("USED");
                OnCardUsed(this);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log(this.gameObject.name + " Was Clicked.");
        }

    }
}
