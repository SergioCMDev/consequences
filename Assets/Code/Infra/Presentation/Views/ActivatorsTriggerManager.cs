﻿using System;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class ActivatorsTriggerManager : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;

        [SerializeField] private GameObject _attachedGameObject;
        [SerializeField] private ParticleSystem _particulas;
        private bool _pushed;


        public event Action<GameObject> OnMoveBlockDown = delegate { };

        private void OnTriggerEnter(Collider other)
        {
            if (!_pushed)
            {
                OnMoveBlockDown.Invoke(_attachedGameObject);
                _pushed = true;
                _particulas.Stop();
            }
        }

        public Vector3 GetAttachedObjectPosition()
        {
            return _attachedGameObject.transform.position;
        }
    }
}