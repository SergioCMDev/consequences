﻿using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DefeatMenuView : MonoBehaviour
{
    [SerializeField] private Button _restartButton, _exitButton;
    [SerializeField] private TextMeshProUGUI _defeatText, _defeatInfo;
    [Inject] private SignalBus _signalBus;

    void Start()
    {
        _restartButton.onClick.AddListener(RestartGame);
        _exitButton.onClick.AddListener(ExitLevel);
    }

    private void ExitLevel()
    {
        
        _signalBus.Fire<ExitGameSignal>();
        _signalBus.Fire<PlayClickSoundSignal>();
    }

    private void RestartGame()
    {
        _signalBus.Fire<RestartGameSignal>();
        _signalBus.Fire<PlayClickSoundSignal>();

    }
    public class Factory : PlaceholderFactory<DefeatMenuView>
    {
    }
}