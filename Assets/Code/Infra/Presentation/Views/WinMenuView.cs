﻿using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class WinMenuView : MonoBehaviour, IInitializable
    {
        [SerializeField] private Button _restartButton, _nextLevelButton, _exitButton;
        [Inject] private SignalBus _signalBus;
        [SerializeField] private GameObject _fullStarImagePrefab, _emptyStarImagePrefab;
        [SerializeField] private Transform _scorePlaceholder;

        // private bool _lastScene = true;

        [Inject]
        public void Construct(int movements, int stars, bool lastScene)
        {
            // //TODO Hacer sistema de puntos basandose en estrellas
            // for (int i = 0; i < 2; i++)
            // {
            //     Instantiate(_fullStarImagePrefab, _scorePlaceholder.transform, false);
            // }
            //
            // for (int i = 0; i < 3; i++)
            // {
            //     Instantiate(_emptyStarImagePrefab, _scorePlaceholder.transform, false);
            // }

            if (lastScene)
            {
                _nextLevelButton.gameObject.SetActive(false);
            }

            Debug.Log(movements);
            Debug.Log(stars);
        }

        void Start()
        {
            _restartButton.onClick.AddListener(RestartGame);
            _nextLevelButton.onClick.AddListener(NextLevel);
            _exitButton.onClick.AddListener(ExitLevel);
        }


        private void NextLevel()
        {
            //TODO
            _signalBus.Fire<PlayClickSoundSignal>();

            _signalBus.Fire<LoadNextLevelSignal>();
        }

        private void ExitLevel()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            _signalBus.Fire<ExitGameSignal>();
        }

        private void RestartGame()
        {
            _signalBus.Fire<PlayClickSoundSignal>();

            _signalBus.Fire<RestartGameSignal>();
        }

        public void Initialize()
        {
        }

        public class Factory : PlaceholderFactory<int, int, bool, WinMenuView>
        {
        }
    }
}