﻿using System.Collections.Generic;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class ActivatorsView : MonoBehaviour
    {
        [SerializeField] private List<ActivatorsTriggerManager> _pushButtonTriggerManagers;
        [Inject] private SignalBus _signalBus;

        void Start()
        {
            foreach (var activators in _pushButtonTriggerManagers)
            {
                activators.OnMoveBlockDown += InitMoveBlockDown;
            }
        }

        private void InitMoveBlockDown(GameObject attachedGameObject)
        {
            _signalBus.Fire(new MoveBlockDownSignal() {Block = attachedGameObject});
            _signalBus.Fire(new DisablePlayerMovementSignal() { });
            _signalBus.Fire<PlayOverButtonSoundSignal>();
        }
    }
}