﻿using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class ExitButtonView : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [SerializeField] private Button _buttonExit;
        void Start()
        {
            _buttonExit.onClick.AddListener(ExitLevelButtonPressed);
        }

        private void ExitLevelButtonPressed()
        {
            _signalBus.Fire(new PlayClickSoundSignal());

            _signalBus.Fire(new PlayGameThemeSoundSignal(){Play = false});

            _signalBus.Fire(new ExitGameSignal());
        }
    }
}
