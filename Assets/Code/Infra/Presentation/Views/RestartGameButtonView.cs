﻿using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class RestartGameButtonView : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [SerializeField] private Button _buttonRestart;
        void Start()
        {
            _buttonRestart.onClick.AddListener(RestartButtonPressed);
        }

        private void RestartButtonPressed()
        {
            _signalBus.Fire(new RestartGameSignal());
            _signalBus.Fire(new PlayClickSoundSignal());
        }
    }
}
