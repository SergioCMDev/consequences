﻿using System;
using Code.Infra.Signals.UpdateViews;
using TMPro;
using UnityEngine;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class MovementsCounterView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _movementText;

        // private string _movementTextValue = "Movements:";


        public void UpdateCounter(int movementsToShow)
        {
            if (movementsToShow <= 0)
            {
                movementsToShow = 0;
            }

            _movementText.SetText(movementsToShow.ToString());
        }
    }
}