﻿using Code.Infra.Signals.UpdateViews;
using ModestTree;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.Infra.Presentation.Views
{
    public class AudioStateView : MonoBehaviour
    {
        [SerializeField]
        private Sprite _audioOn, _audioOff;

        [SerializeField] private Button _audioObject;
        [SerializeField] private Image _audioImageObject;

        [Inject] private SignalBus _signalBus;

        void Start()
        {
            Assert.IsNotNull(_audioOn);
            Assert.IsNotNull(_audioOff);
            Assert.IsNotNull(_audioObject);
            _audioObject.onClick.AddListener(ChangeAudioButtonPressed);
        }

        private void ChangeAudioButtonPressed()
        {
            _signalBus.Fire(new AudioStateChangedSignal());
        }

        public void SetEnableAudioImage()
        {
            _audioImageObject.sprite = _audioOn;
        }

        public void SetDisableAudioImage()
        {
            _audioImageObject.sprite = _audioOff;
        }
    }
}
