﻿using TMPro;
using UnityEngine;

namespace Code.Infra.Presentation.Views
{
    public class StarCounterView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _starCounterText;

        public void UpdateCounter(int starsToShow)
        {
            _starCounterText.SetText(starsToShow.ToString());
        }
    }
}
