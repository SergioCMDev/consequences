﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Infra.Presentation.Views
{
    public class StartMenuView : MonoBehaviour
    {
        [SerializeField] private Button _playButton, _buttonContinue, _buttonLevelSelection;
        public event Action OnSelectionLevelButtonPressed;
        public event Action OnLoadCurrentLevel;
        public event Action OnTutorialLoad;

        //TODO Finish
        void Start()
        {
            _playButton.onClick.AddListener(LoadTutorial);
            _buttonContinue.onClick.AddListener(LoadCurrentLevel);
            _buttonLevelSelection.onClick.AddListener(OpenSelectionLevelsMenu);
        }

        public void ShowPlayButton()
        {
            _playButton.gameObject.SetActive(true);
            _buttonContinue.gameObject.SetActive(false);
            _buttonLevelSelection.gameObject.SetActive(false);
        }

        public void ShowOthersButtons()
        {
            _playButton.gameObject.SetActive(false);
            _buttonContinue.gameObject.SetActive(true);
            _buttonLevelSelection.gameObject.SetActive(true);
        }


        private void LoadTutorial()
        {
            OnTutorialLoad.Invoke();
        }

        private void LoadCurrentLevel()
        {
            OnLoadCurrentLevel.Invoke();
        }

        private void OpenSelectionLevelsMenu()
        {
            OnSelectionLevelButtonPressed.Invoke();
        }

        void OnDestroy()
        {
            _playButton.onClick.RemoveListener(OpenSelectionLevelsMenu);
            _buttonContinue.onClick.RemoveListener(LoadCurrentLevel);
            _buttonLevelSelection.onClick.RemoveListener(OpenSelectionLevelsMenu);
        }
    }
}