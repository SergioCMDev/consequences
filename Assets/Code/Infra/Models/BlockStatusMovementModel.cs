﻿using Code.Infra.Interfaces;

namespace Code.Infra.Models
{
    public class BlockStatusMovementModel : IBlockStatusMovementModel
    {
        private bool _blockMoving;
        public bool IsBlockMoving()
        {
            return _blockMoving;
        }

        public void BlockStartMoving()
        {
            _blockMoving = true;
        }

        public void BlockStopMoving()
        {
            _blockMoving = false;
        }
    }
}