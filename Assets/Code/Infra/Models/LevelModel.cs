﻿using Code.Domain.Levels;
using Code.Infra.Dto;
using Code.Infra.Interfaces;
using Zenject;
using SceneManager = Code.Domain.Scenes.SceneManager;

namespace Code.Infra.Models
{
    public class LevelModel : ILevelModel, IInitializable
    {
        [Inject] private LevelsDataManager _levelsDataManager;

        [Inject] private SceneManager _sceneManager;
        public LevelDto LevelDto { get; private set; }
        private int MovementsTutorial = 20;
        public void Initialize()
        {
            LevelDto = new LevelDto();
        }

        public void UpdateMaxMovements(string sceneName)
        {
            if (sceneName == "Tutorial")
            {
                LevelDto.MaxMovements = MovementsTutorial;
                return;
            }

            LevelDto.MaxMovements = _levelsDataManager
                .GetAllLevelDataBySceneName(sceneName).MaxMovements;
        }
    }
}