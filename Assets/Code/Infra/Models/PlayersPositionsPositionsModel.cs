﻿using Code.Infra.Interfaces;
using UnityEngine;

namespace Code.Infra.Models
{
    public class PlayersPositionsPositionsModel : IPlayersPositionsModel
    {
        public Vector3 PositionPlayerOne { get; set; }
        public Vector3 PositionPlayerTwo { get; set; }
    }
}