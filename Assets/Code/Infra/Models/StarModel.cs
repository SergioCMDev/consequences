﻿using Code.Infra.Interfaces;
using Zenject;

namespace Code.Infra.Models
{
    public class StarModel : IStarModel, IInitializable
    {
        private int _stars;
        public int GetStars()
        {
            return _stars;
        }

        public void Initialize()
        {
            _stars = 0;
        }

        public void UpdateCounter()
        {
            _stars++;
        }
    }
}
