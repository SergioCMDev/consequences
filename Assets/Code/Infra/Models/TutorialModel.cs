﻿using Code.Infra.Interfaces;
using Code.Utilities;
using UnityEngine;
using Zenject;

namespace Code.Infra.Models
{
    public class TutorialModel : ITutorialModel, IInitializable
    {
        private bool _tutorialDone;

        public void Initialize()
        {
            int tutorialState = PlayerPrefs.GetInt(Constants.Tutorial.TUTORIAL_VALUE_NAME);

            _tutorialDone = tutorialState == 1;
        }

        public void SetTutorialDone()
        {
            _tutorialDone = true;
            PlayerPrefs.SetInt(Constants.Tutorial.TUTORIAL_VALUE_NAME, 1);
        }

        public bool IsTutorialDone()
        {
            return _tutorialDone;
        }
    }
}