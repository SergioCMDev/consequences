﻿using Code.Utilities;
using UnityEngine;

namespace Code.Infra.Models
{
    public enum TypeMovement
    {
        FORWARD,
        RIGHT,
        LEFT,
        BACK,
        RETURN
    };

    public class MovementInfo
    {
        private float _quantityMove;
        private TypeMovement _type;
        private float _rotation;
        private Vector3 _orientation;
        private Vector3 _origin;
        private Vector3 _destination;

        public MovementInfo(TypeMovement TypeMovement, Vector3 OriginPosition, float QuantityMovement = 1)
        {
            _quantityMove = QuantityMovement;
            _type = TypeMovement;
            _origin = OriginPosition;
            CalculateRotation();
        }

        public float Rotation
        {
            get => _rotation;
        }

        public Vector3 Destination
        {
            get => _destination;
        }

        public void CalculateDestination(Vector3 forwardVectorPlayer)
        {
            _destination = _origin + forwardVectorPlayer * _quantityMove;
        }

        public void SetOrientation(Vector3 forwardVectorPlayer)
        {
            _orientation = forwardVectorPlayer + new Vector3(0, _rotation, 0);
        }

        public void SetReverseMove()
        {
            Vector3 previousDestination = _destination;
            _destination = _origin;
            _origin = _destination;
            _orientation *= -1;
        }

        private void CalculateRotation()
        {
            switch (_type)
            {
                case TypeMovement.FORWARD:
                    _rotation = Constants.MovementsRotation.ROTATION_FORWARD;
                    break;
                case TypeMovement.RIGHT:
                    _rotation = Constants.MovementsRotation.ROTATION_RIGHT;
                    break;
                case TypeMovement.LEFT:
                    _rotation = Constants.MovementsRotation.ROTATION_LEFT;
                    break;
                case TypeMovement.BACK:
                    _rotation = Constants.MovementsRotation.ROTATION_BACK;
                    break;
            }
        }

        public void ReverseRotation()
        {
            switch (_type)
            {
                case TypeMovement.FORWARD:
                    _rotation = Constants.MovementsRotation.ROTATION_BACK;
                    break;
                case TypeMovement.RIGHT:
                    _rotation = Constants.MovementsRotation.ROTATION_LEFT;
                    break;
                case TypeMovement.LEFT:
                    _rotation = Constants.MovementsRotation.ROTATION_RIGHT;
                    break;
                case TypeMovement.BACK:
                    _rotation = Constants.MovementsRotation.ROTATION_FORWARD;
                    break;
            }
        }
    }
}