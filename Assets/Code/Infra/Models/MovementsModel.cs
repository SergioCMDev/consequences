﻿using Code.Infra.Interfaces;
using UnityEngine;
using Zenject;

namespace Code.Infra.Models
{
    public class MovementsModel : IMovementsModel, IInitializable
    {
        private int _movements;
        private bool _enableMovements;

        public int GetMovements()
        {
            return _movements;
        }
        
        public void Initialize()
        {
            _movements = 0;
            EnableMovements();
        }

        public int RemainingMovements { get; set; }

        public void UpdateCounter(int numMoves)
        {
            _movements+=numMoves;
        }

        public void RemoveCounter()
        {
            _movements--;
        }

        public void DisableMovements()
        {
            // Debug.Log("PLAYERS CANT MOVE");
            _enableMovements = false;
        }

        public void EnableMovements()
        {
            _enableMovements = true;
        }

        public bool CanMove()
        {
            return _enableMovements;
        }
    }
}
