﻿using System.Collections.Generic;
using Code.Infra.Interfaces;

namespace Code.Infra.Models
{
    public class GoalsModel : IGoalsModel
    {
        private List<PlayerBase> _playersOverGoal = new List<PlayerBase>(2);
        public void PlayerReachGoal(PlayerBase playerOverGoal)
        {
            _playersOverGoal.Add(playerOverGoal);
        }

        public void PlayerLeaveGoal(PlayerBase playerOverGoal)
        {
            _playersOverGoal.Clear();
        }
        public int GetPlayersAtGoal()
        {
            return _playersOverGoal.Count;
        }

        public void ResetPlayersAtGoal()
        {
            _playersOverGoal.Clear();
        }
    }
}