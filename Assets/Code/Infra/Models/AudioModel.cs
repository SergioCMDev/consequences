﻿using Code.App.Controllers;
using Code.Domain.Interfaces;
using Code.Enums;
using Zenject;

namespace Code.Infra.Models
{
    public class AudioModel : IAudioModel, IInitializable
    {
        private AudioState _generalAudioState;
        private AudioState _fxAudioState;
        [Inject] private AudioController _audioController;
        public void ChangeAudioState()
        {
            _generalAudioState = _generalAudioState == AudioState.Enabled ? AudioState.Disabled : AudioState.Enabled;
            _audioController.SetAudioState(_generalAudioState == AudioState.Disabled);
        }

        public AudioState GetStateAudio()
        {
            return _generalAudioState;
        }

        public void Initialize()
        {
            _generalAudioState = AudioState.Enabled;
            _fxAudioState = AudioState.Enabled;
        }
    }
}
