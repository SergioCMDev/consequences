﻿using System;
using System.Collections.Generic;
using Code.Infra.Interfaces;
using Code.Infra.Signals.Actions;
using Code.Managers.Coroutines;
using Code.Utilities;
using ScriptableObjects.Templates;
using UnityEngine;
using Zenject;

namespace Code.Infra.Models
{
    public abstract class PlayerBase : MonoBehaviour
    {
        [SerializeField] protected PlayerInfo playerInfo;
        [SerializeField] private Animator animator;
        [Inject] protected SignalBus _signalBus;
        [Inject] protected IBlocksPositionModel _blocksPositionModel;
        [Inject] private IMovementsModel _movementsModel;
        private MovementInfo lastMovement;
        protected List<MovementInfo> _movesToDO = new List<MovementInfo>();


        private Vector3 GetLastPosition()
        {
            return _movesToDO.Count > 0 ? _movesToDO[_movesToDO.Count - 1].Destination : transform.position;
        }


        protected int GetNumOfMoves(TypeMovement objTypeMovement, MovementInfo movementInfo)
        {
            Vector3 playerPosition = transform.position;
            int xPlayer = Mathf.RoundToInt(playerPosition.x);
            int zPlayer = Mathf.RoundToInt(playerPosition.z);
            int xDestination = Mathf.RoundToInt(movementInfo.Destination.x);
            int zDestination = Mathf.RoundToInt(movementInfo.Destination.z);
            switch (objTypeMovement)
            {
                case TypeMovement.FORWARD:
                case TypeMovement.BACK:
                    return Math.Abs(xDestination - xPlayer);
                case TypeMovement.RIGHT:
                case TypeMovement.LEFT:
                    return Math.Abs(zDestination - zPlayer);
                default:
                    throw new ArgumentOutOfRangeException(nameof(objTypeMovement), objTypeMovement, null);
            }
        }

        protected void CheckPositionsToMove(int numMoves, TypeMovement typeMovement)
        {
            _movesToDO.Clear();
            if (_movementsModel.RemainingMovements - numMoves < 0)
            {
                numMoves = _movementsModel.RemainingMovements;
            }
            
            for (int i = 0; i < numMoves; i++)
            {
                Vector3 previousEulerAngles = transform.eulerAngles;
                MovementInfo movementInfo = GetNewMove(typeMovement, GetLastPosition());

                transform.eulerAngles = new Vector3(0, movementInfo.Rotation, 0);
                movementInfo.CalculateDestination(transform.forward);
                movementInfo.SetOrientation(transform.forward);

                if (ValidPosition(movementInfo.Destination))
                {
                    _movesToDO.Add(movementInfo);
                }

                transform.eulerAngles = previousEulerAngles;
            }
        }

        protected void SetPlayerMove(TypeMovement typeMovement)
        {
            if (typeMovement == TypeMovement.RETURN)
            {
                ReverseMove();
                return;
            }

            if (_movesToDO.Count > 0)
            {
                lastMovement = _movesToDO[_movesToDO.Count - 1];
                Move(lastMovement);
                return;
            }
            
            _signalBus.Fire(new PlayerStopMovingSignal(){ Player = this});
        }

        private void ReverseMove()
        {
            lastMovement.SetReverseMove();

            transform.eulerAngles = new Vector3(0, lastMovement.Rotation, 0);

            lastMovement.SetOrientation(transform.forward);
            lastMovement.ReverseRotation();
            Move(lastMovement);
        }

        private MovementInfo GetNewMove(TypeMovement typeMovement, Vector3 position, int numMoves = 1)
        {
            MovementInfo movInfo = new MovementInfo(typeMovement, position, numMoves);

            return movInfo;
        }

        private bool ValidPosition(Vector3 positionToCheck)
        {
            if (_blocksPositionModel.BlockInPosition(positionToCheck))
            {
                return false;
            }

            RaycastHit hitSuelo;
            Debug.DrawRay(positionToCheck + Vector3.up * 0.5f, -transform.up, Color.red, 2.0f);
            Physics.Raycast(positionToCheck+ Vector3.up * 0.5f , -transform.up, out hitSuelo, 1.0f,
                playerInfo.layersToCheckGround);

            if (hitSuelo.collider)
            {
                return true;
            }

            return false;
        }

        private void Move(MovementInfo movement)
        {
            animator.SetBool(Constants.AnimationsValues.MOVE, true);
            transform.eulerAngles = new Vector3(0, movement.Rotation, 0);
            MoveCoroutine m = new MoveCoroutine(transform, transform.position, movement.Destination,
                playerInfo.SpeedMovement);
            m.OnCoroutineEnd += OnPlayerEndMoving;
            CoroutineManager.Instance.InitCoroutine(m.Start());
        }

        private void OnPlayerEndMoving(MoveCoroutine m)
        {
            animator.SetBool(Constants.AnimationsValues.MOVE, false);
            m.OnCoroutineEnd -= OnPlayerEndMoving;
            _signalBus.Fire(new PlayerStopMovingSignal() {Player = this});
        }


        protected TypeMovement GetReverseMove(TypeMovement objTypeMovement)
        {
            switch (objTypeMovement)
            {
                case TypeMovement.FORWARD:
                    return TypeMovement.BACK;
                case TypeMovement.BACK:
                    return TypeMovement.FORWARD;
                case TypeMovement.RIGHT:
                    return TypeMovement.LEFT;
                case TypeMovement.LEFT:
                    return TypeMovement.RIGHT;
                default:
                    throw new ArgumentOutOfRangeException(nameof(objTypeMovement), objTypeMovement, null);
            }
        }

        private void OnDrawGizmos()
        {
            Debug.DrawRay(transform.position + Vector3.up * 0.5f + transform.forward, transform.forward, Color.green,
                1f);
            Debug.DrawRay(transform.position + Vector3.up * 0.5f, transform.forward, Color.blue, 1f);
            Debug.DrawRay(transform.position + Vector3.up * 0.5f + transform.forward * 1, -transform.up, Color.yellow);
        }
    }
}