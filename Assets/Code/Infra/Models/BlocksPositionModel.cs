﻿using System.Collections.Generic;
using Code.Infra.Interfaces;
using UnityEngine;
using Zenject;

namespace Code.Infra.Models
{
    public class BlocksPositionModel : IBlocksPositionModel
    {
        private List<Vector3> _blockPositions;

        [Inject]
        public void Construct()
        {
            _blockPositions = new List<Vector3>();
        }

        public bool BlockInPosition(Vector3 origin)
        {
            Vector3 vectorToCompare = new Vector3(Mathf.RoundToInt(origin.x), Mathf.RoundToInt(origin.y),
                Mathf.RoundToInt(origin.z));
            return _blockPositions.Contains(vectorToCompare);
        }

        public void AddBlock(Vector3 position)
        {
            _blockPositions.Add(position);
        }

        public void CleanBlocks()
        {
            _blockPositions.Clear();
        }

        public void RemoveBlock(Vector3 position)
        {
            Vector3 vectorToRemove = new Vector3(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y),
                Mathf.RoundToInt(position.z));
            _blockPositions.Remove(vectorToRemove);
        }
    }
}