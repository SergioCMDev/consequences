﻿namespace Code.Infra.Dto
{
    public class LevelDto
    {
        public int Id;
        public int Movements;
        public int MaxMovements;
        public int StarsAcheived;
        public int TotalStarsAtLevel;
        public string SceneName;
        public string LevelName;
        public bool Unlocked;
        public string ImagePath;
    }
}