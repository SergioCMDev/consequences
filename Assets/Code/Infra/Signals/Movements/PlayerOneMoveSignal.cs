﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Movements
{
    public class PlayerOneMoveSignal
    {
        public TypeMovement TypeMovement;
    }
}
