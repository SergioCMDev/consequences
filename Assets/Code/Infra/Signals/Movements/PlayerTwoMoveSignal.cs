﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Movements
{
    public class PlayerTwoMoveSignal
    {
        public TypeMovement TypeMovement;
    }
}
