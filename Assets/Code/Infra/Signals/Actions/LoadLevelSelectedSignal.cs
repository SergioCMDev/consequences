﻿namespace Code.Infra.Signals.Actions
{
    public class LoadLevelSelectedSignal
    {
        public string SceneName;
    }
}