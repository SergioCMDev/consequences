﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Actions
{
    public class PlayerStopMovingSignal
    {
        public PlayerBase Player;
    }
}