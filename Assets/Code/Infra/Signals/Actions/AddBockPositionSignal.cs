﻿using UnityEngine;

namespace Code.Infra.Signals.Actions
{
    public class AddBockPositionSignal
    {
        public Vector3 BlockPositionToAdd;
    }
}