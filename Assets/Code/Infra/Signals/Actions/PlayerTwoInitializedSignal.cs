﻿using UnityEngine;

namespace Code.Infra.Signals.Actions
{
    public class PlayerTwoInitializedSignal
    {
        public Vector3 InitialPosition;
    }
}