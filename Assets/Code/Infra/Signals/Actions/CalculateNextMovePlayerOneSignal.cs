﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Actions
{
    public class CalculateNextMovePlayerOneSignal
    {
        public TypeMovement TypeMovement;
        public int NumMoves;
    }
}