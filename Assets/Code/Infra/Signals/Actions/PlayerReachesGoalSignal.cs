﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Actions
{
    public class PlayerReachesGoalSignal
    {
        public PlayerBase PlayerBase;
    }
}