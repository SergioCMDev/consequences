﻿using UnityEngine;

namespace Code.Infra.Signals.Actions
{
    public class MoveToNewCubeSignal
    {
        public Vector3 CubePosition;
    }
}