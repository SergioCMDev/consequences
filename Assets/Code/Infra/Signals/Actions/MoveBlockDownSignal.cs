﻿using UnityEngine;

namespace Code.Infra.Signals.Actions
{
    public class MoveBlockDownSignal
    {
        public GameObject Block;
    }
}