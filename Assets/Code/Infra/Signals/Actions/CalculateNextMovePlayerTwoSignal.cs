﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Actions
{
    public class CalculateNextMovePlayerTwoSignal
    {
        public TypeMovement TypeMovement;
        public int NumMoves;
    }
}