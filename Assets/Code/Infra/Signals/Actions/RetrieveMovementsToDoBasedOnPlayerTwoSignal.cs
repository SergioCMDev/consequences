﻿using Code.Infra.Models;

namespace Code.Infra.Signals.Actions
{
    public class RetrieveMovementsToDoBasedOnPlayerTwoSignal
    {
        public TypeMovement InfoMovement;
        public int NumMovesPlayerOne;
    }
}