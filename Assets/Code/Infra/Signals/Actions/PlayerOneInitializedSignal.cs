﻿using UnityEngine;

namespace Code.Infra.Signals.Actions
{
    public class PlayerOneInitializedSignal
    {
        public Vector3 InitialPosition;
    }
}