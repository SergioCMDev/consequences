﻿namespace Code.Infra.Signals.Actions
{
    public class WinGameSignal
    {
        public string SceneName;
        public int Stars;
        public int Movements;
    }
}