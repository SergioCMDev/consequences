﻿namespace Code.Infra.Signals.UpdateViews
{
    public class LoadMovementsSignal
    {
        public int Movements;
    }
}