﻿using UnityEngine;

namespace Code.Infra.Interfaces
{
    public interface IPlayersPositionsModel
    {
        Vector3 PositionPlayerOne { get; set; }
        Vector3 PositionPlayerTwo { get; set; }
    }
}