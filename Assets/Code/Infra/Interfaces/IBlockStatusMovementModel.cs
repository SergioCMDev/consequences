﻿namespace Code.Infra.Interfaces
{
    public interface IBlockStatusMovementModel
    {
        bool IsBlockMoving();
        void BlockStartMoving();
        void BlockStopMoving();
    }
}