﻿namespace Code.Infra.Interfaces
{
    public interface ITutorialModel
    {
        void SetTutorialDone();
        bool IsTutorialDone();
    }
}