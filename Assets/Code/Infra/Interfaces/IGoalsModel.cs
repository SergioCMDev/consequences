﻿using Code.Infra.Models;

namespace Code.Infra.Interfaces
{
    public interface IGoalsModel
    {
        void PlayerReachGoal(PlayerBase playerOverGoal);
        void PlayerLeaveGoal(PlayerBase playerOverGoal);
        int GetPlayersAtGoal();
        void ResetPlayersAtGoal();
    }
}