﻿namespace Code.Infra.Interfaces
{
    public interface IMovementsModel
    {
        int RemainingMovements { get; set; }
        void UpdateCounter(int numMoves);
        void DisableMovements();
        void EnableMovements();
        void RemoveCounter();
        bool CanMove();
        void Initialize();
        int GetMovements();
    }
}