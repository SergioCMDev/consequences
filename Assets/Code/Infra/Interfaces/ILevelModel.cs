﻿using Code.Infra.Dto;

namespace Code.Infra.Interfaces
{
    public interface ILevelModel
    {
        LevelDto LevelDto { get; }
        void UpdateMaxMovements(string SceneName);
    }
}