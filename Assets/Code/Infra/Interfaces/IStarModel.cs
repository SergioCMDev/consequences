﻿namespace Code.Infra.Interfaces
{
    public interface IStarModel
    {
        void UpdateCounter();
        void Initialize();
        int GetStars();
    }
}