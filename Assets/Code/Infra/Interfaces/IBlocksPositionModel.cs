﻿using UnityEngine;

namespace Code.Infra.Interfaces
{
    public interface IBlocksPositionModel
    {
        bool BlockInPosition(Vector3 position);
        void RemoveBlock(Vector3 position);
        void AddBlock(Vector3 position);
        void CleanBlocks();
    }
}