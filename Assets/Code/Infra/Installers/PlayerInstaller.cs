using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Movements;
using Zenject;

namespace Code.Infra.Installers
{
    public class PlayerInstaller : MonoInstaller<PlayerInstaller>
    {
        public override void InstallBindings()
        {
            BindSignals();
            BindControllers();
        }

        private void BindControllers()
        {
        }

        private void BindSignals()
        {
            Container.DeclareSignal<MoveForwardSignal>();
            Container.DeclareSignal<MoveBackwardSignal>();
            Container.DeclareSignal<MoveLeftSignal>();
            Container.DeclareSignal<MoveRightSignal>();
            Container.DeclareSignal<EnablePlayerMovementSignal>();
            Container.DeclareSignal<DisablePlayerMovementSignal>();
            Container.DeclareSignal<PlayerOneMoveSignal>();
            Container.DeclareSignal<PlayerTwoMoveSignal>();
            Container.DeclareSignal<PlayerReachesGoalSignal>();
            Container.DeclareSignal<PlayerLeavesGoalSignal>();
            Container.DeclareSignal<PlayerStopMovingSignal>();
            Container.DeclareSignal<PlayerOneInitializedSignal>();
            Container.DeclareSignal<PlayerTwoInitializedSignal>();
            Container.DeclareSignal<CalculateNextMovePlayerTwoSignal>();
            Container.DeclareSignal<CalculateNextMovePlayerOneSignal>();
            Container.DeclareSignal<RetrieveMovementsToDoBasedOnPlayerTwoSignal>();
            
            
        }
    }
}