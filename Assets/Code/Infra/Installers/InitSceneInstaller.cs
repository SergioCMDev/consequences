﻿using Code.Infra.Presentation.InitMenu;
using UnityEngine;
using Zenject;

namespace Code.Infra.Installers
{
    public class InitSceneInstaller : MonoInstaller<InitSceneInstaller>
    {
        [SerializeField] private InitMenuMediator _initMenuMediator;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<InitMenuSceneController>().AsSingle();
            Container.BindInterfacesAndSelfTo<InitMenuMediator>().FromInstance(_initMenuMediator);
        }
    }
}