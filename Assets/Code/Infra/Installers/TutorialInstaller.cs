﻿using Code.Infra.Presentation.Presenters.Tutorial;
using Code.Infra.Presentation.Views;
using UnityEngine;
using Zenject;

namespace Code.Infra.Installers
{
    public class TutorialInstaller : MonoInstaller<TutorialInstaller>
    {
        [SerializeField] private TutorialView _tutorialView;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<TutorialPresenter>().AsSingle();
            Container.BindInterfacesAndSelfTo<TutorialView>().FromInstance(_tutorialView);
        }
    }
}