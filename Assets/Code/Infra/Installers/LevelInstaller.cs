using Code.App.Controllers;
using Code.Infra.Models;
using Code.Infra.Presentation.Presenters;
using Code.Infra.Signals.Actions;
using Code.Infra.Signals.UpdateViews;
using Zenject;
using SceneManager = Code.Domain.Scenes.SceneManager;

namespace Code.Infra.Installers
{
    public class LevelInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            DeclareSignals();
            BindModels();
            BindControllers();
            BindViews();
        }

        private void BindViews()
        {
            // Container.BindInterfacesAndSelfTo<CanvasPresenter>().AsSingle();
        }

        private void BindControllers()
        {
            Container.BindInterfacesAndSelfTo<BlocksManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<GoalsController>().AsSingle();
            Container.BindInterfacesAndSelfTo<WinGameController>().AsSingle();
        }

        private void BindModels()
        {
            Container.BindInterfacesAndSelfTo<StarModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<MovementsModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayersPositionsPositionsModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<GoalsModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<AudioModel>().AsSingle();

            Container.BindInterfacesAndSelfTo<LevelModel>().AsSingle();

            Container.BindInterfacesAndSelfTo<SceneManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<TutorialModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<BlocksPositionModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<BlockStatusMovementModel>().AsSingle();
        }

        private void DeclareSignals()
        {
            Container.DeclareSignal<PlayerGetStarSignal>();
            Container.DeclareSignal<RestartGameSignal>();
            Container.DeclareSignal<WinGameSignal>();
            Container.DeclareSignal<ExitGameSignal>();
            Container.DeclareSignal<MoveBlockDownSignal>();
            Container.DeclareSignal<AudioStateChangedSignal>();
            Container.DeclareSignal<LoadMovementsSignal>();
            Container.DeclareSignal<LoadNextLevelSignal>();
            Container.DeclareSignal<LoadTutorialSignal>();
            Container.DeclareSignal<LoadLevelSelectedSignal>();
            Container.DeclareSignal<TutorialCompletedSignal>();
            Container.DeclareSignal<MoveToNewCubeSignal>();
            Container.DeclareSignal<AddBockPositionSignal>();
            Container.DeclareSignal<BlockStopMovingSignal>();
            Container.DeclareSignal<EndTutorialExplicationSignal>();
            Container.DeclareSignal<LostGameSignal>();
        }
    }
}