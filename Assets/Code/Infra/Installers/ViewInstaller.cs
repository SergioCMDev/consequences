using Code.App.Controllers;
using Code.Infra.Presentation.Presenters;
using Code.Infra.Presentation.Views;
using UnityEngine;
using Zenject;

namespace Code.Infra.Installers
{
    public class ViewInstaller : MonoInstaller
    {
        [SerializeField] private AudioStateView _audioStateView;
        [SerializeField] private StarCounterView _starCounterView;
        [SerializeField] private MovementsCounterView _movementsCounterView;

        [SerializeField] private GameObject _winMenuViewPrefab;
        [SerializeField] private GameObject _deafeatMenuViewPrefab;
        [SerializeField] public Canvas _canvas;

        public override void InstallBindings()
        {
            BindViews();
            BindControllers();
        }

        private void BindControllers()
        {
            Container.BindInterfacesAndSelfTo<StarsController>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayersMovementController>().AsSingle();
            Container.BindInterfacesAndSelfTo<CanvasController>().AsSingle();
        }

        private void BindViews()
        {
            Container.BindInterfacesAndSelfTo<AudioStateView>().FromInstance(_audioStateView).AsSingle();
            Container.BindInterfacesAndSelfTo<StarCounterView>().FromInstance(_starCounterView).AsSingle();
            Container.BindInterfacesAndSelfTo<MovementsCounterView>().FromInstance(_movementsCounterView).AsSingle();
            Container.Bind<Canvas>().FromInstance(_canvas).AsSingle();


            Container.BindFactory<int, int, bool, WinMenuView, WinMenuView.Factory>().FromComponentInNewPrefab(
                _winMenuViewPrefab);
            Container.BindFactory<DefeatMenuView, DefeatMenuView.Factory>()
                .FromComponentInNewPrefab(_deafeatMenuViewPrefab);
        }
    }
}