﻿using Code.Infra.Signals.Sounds;
using UnityEngine;
using Zenject;

namespace Code.App.Installers
{
    public class AudioInstaller : MonoInstaller<AudioInstaller>
    {
        [SerializeField] private GameObject _audioMixerPrefab;

        public override void InstallBindings()
        {
            BindSignals();
            Container.BindFactory<AudioMixerPlayer, AudioMixerPlayer.Factory>()
                .FromComponentInNewPrefab(_audioMixerPrefab);
        }

        private void BindSignals()
        {
            Container.DeclareSignal<PlayClickSoundSignal>();
            Container.DeclareSignal<PlayVictorySoundSignal>();
            Container.DeclareSignal<PlayLostSoundSignal>();
            Container.DeclareSignal<PlayStartLevelSoundSignal>();
            Container.DeclareSignal<PlayCollectStarSoundSignal>();
            Container.DeclareSignal<PlayMenuThemeSoundSignal>();
            Container.DeclareSignal<PlayGameThemeSoundSignal>();
            Container.DeclareSignal<PlayOverButtonSoundSignal>();
            
        }
    }
}