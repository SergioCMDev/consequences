﻿using Code.Domain.Interfaces;
using Code.Infra.Interfaces;
using Code.Infra.Models;
using Code.Infra.Presentation.Presenters;
using Code.Infra.Signals.Actions;
using Code.Utilities;
using UnityEngine;
using Zenject;

namespace Code.App.Controllers
{
    public class GoalsController : IInitializable
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private IGoalsModel _goalsModel;
        [Inject] private IMovementsModel _movementsModel;
        [Inject] private IStarModel _starModel;
        [Inject] private ISceneManager _sceneManager;
        [Inject] private IPlayersPositionsModel _playersPositionsModel;
        private const int NUM_PLAYERS = 2;
        private int playersStopped = 0;

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerReachesGoalSignal>(PlayerReachesGoal);
            _signalBus.Subscribe<PlayerLeavesGoalSignal>(PlayerLeavesGoal);
            _signalBus.Subscribe<PlayerStopMovingSignal>(PlayerStopMoving);
        }

        private void PlayerStopMoving(PlayerStopMovingSignal obj)
        {
            playersStopped++;
            SetPositionsOfPlayers(obj.Player);
   
            if (playersStopped == NUM_PLAYERS)
            {
                Debug.Log("PLAYERS STOPPED");
                playersStopped = 0;
                if (_goalsModel.GetPlayersAtGoal() == NUM_PLAYERS)
                {
                    _goalsModel.ResetPlayersAtGoal();

                    if (_sceneManager.GetActualScene().name == Constants.Tutorial.TUTORIAL_SCENE_NAME)
                    {
                        _signalBus.Fire(new TutorialCompletedSignal());
                        _signalBus.Fire<DisablePlayerMovementSignal>();
                        return;
                    }

                    _signalBus.Fire(new WinGameSignal()
                    {
                        SceneName = _sceneManager.GetActualScene().name,

                        Movements = _movementsModel.GetMovements(),
                        Stars = _starModel.GetStars()
                    });
                    _signalBus.Fire<DisablePlayerMovementSignal>();
                }
                else if (_movementsModel.RemainingMovements <= 0)
                {
                    _signalBus.Fire<DisablePlayerMovementSignal>();

                    _signalBus.Fire(new LostGameSignal() { });
                    Debug.Log("FIN");
                }
            }
        }

        private void SetPositionsOfPlayers(PlayerBase objPlayer)
        {
            if (objPlayer.GetType() == typeof(PlayerOneMovementBehaviour))
            {
                _playersPositionsModel.PositionPlayerOne = objPlayer.transform.position;
            }
            else if (objPlayer.GetType() == typeof(PlayerTwoMovementBehaviour))
            {
                _playersPositionsModel.PositionPlayerTwo = objPlayer.transform.position;
            }
        }

        private void PlayerReachesGoal(PlayerReachesGoalSignal obj)
        {
            // _goalsModel.ResetPlayersAtGoal();

            _goalsModel.PlayerReachGoal(obj.PlayerBase);
        }

        private void PlayerLeavesGoal(PlayerLeavesGoalSignal obj)
        {
            _goalsModel.ResetPlayersAtGoal();

            _goalsModel.PlayerLeaveGoal(obj.PlayerBase);
        }
    }
}