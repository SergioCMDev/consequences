﻿using System;
using Code.Domain.Interfaces;
using Code.Infra.Signals.Actions;
using Zenject;

namespace Code.App.Controllers
{
    public class SceneManager : IInitializable, IDisposable
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private ISceneManager _sceneManager;

        public void Initialize()
        {
            _signalBus.Subscribe<RestartGameSignal>(RestartGame);
            _signalBus.Subscribe<LoadNextLevelSignal>(LoadNextLevel);
            _signalBus.Subscribe<LoadTutorialSignal>(LoadTutorial);
            _signalBus.Subscribe<ExitGameSignal>(ExitGame);
            _signalBus.Subscribe<LoadLevelSelectedSignal>(LoadLevelSelected);
        }

        private void LoadLevelSelected(LoadLevelSelectedSignal signal)
        {
            _sceneManager.ChangeToScene(signal.SceneName);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<RestartGameSignal>(RestartGame);
            _signalBus.Unsubscribe<LoadNextLevelSignal>(LoadNextLevel);
            _signalBus.Unsubscribe<LoadTutorialSignal>(LoadTutorial);
            _signalBus.Unsubscribe<ExitGameSignal>(ExitGame);
        }

        private void LoadTutorial(LoadTutorialSignal obj)
        {
            _sceneManager.LoadTutorialScene();
        }

        private void LoadNextLevel(LoadNextLevelSignal obj)
        {
            _sceneManager.LoadNexLevel();
        }
        
        private void ExitGame(ExitGameSignal signal)
        {
            _sceneManager.ChangeToScene("InitScene");
        }

        private void RestartGame(RestartGameSignal signal)
        {
            _sceneManager.RestartScene();
        }
    }
}