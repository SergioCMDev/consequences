﻿using System;
using Code.Domain.Interfaces;
using Code.Infra.Dto;
using Code.Infra.Signals.Actions;
using Zenject;

namespace Code.App.Controllers
{
    public class WinGameController : IInitializable, IDisposable
    {
        [Inject] private ILevelsDataManager _levelsDataManager;
        [Inject] private SignalBus _signalBus;

        public void Initialize()
        {
            _signalBus.Subscribe<WinGameSignal>(SaveLevelData);
            _signalBus.Subscribe<WinGameSignal>(UnlockNextLevel);
        }

        private void UnlockNextLevel(WinGameSignal obj)
        {
            _levelsDataManager.UnlockNextLevel();
        }

        private void SaveLevelData(WinGameSignal obj)
        {
            LevelDto levelData = new LevelDto()
            {
                SceneName = obj.SceneName,
                StarsAcheived = obj.Stars,
                Movements = obj.Movements
            };
            _levelsDataManager.SaveLevelData(levelData);
        }


        public void Dispose()
        {
            _signalBus.Unsubscribe<WinGameSignal>(SaveLevelData);
            _signalBus.Unsubscribe<WinGameSignal>(UnlockNextLevel);
        }
    }
}