﻿using Code.Infra.Signals.Actions;
using Code.Infra.Signals.Sounds;
using Zenject;

namespace Code.App.Controllers
{
    public class AudioController : IInitializable
    {
        [Inject] private SignalBus _signalBus;

        [Inject] readonly AudioMixerPlayer.Factory _audioMixerFactoy;

        // [Inject] private AudioStateView audioStateView;
        private AudioMixerPlayer _audioMixer;

        public void Initialize()
        {
            _signalBus.Subscribe<PlayVictorySoundSignal>(PlayVictorSound);
            _audioMixer = _audioMixerFactoy.Create();


            _signalBus.Subscribe<PlayClickSoundSignal>(PlayClickSound);
            _signalBus.Subscribe<PlayMenuThemeSoundSignal>(PlayMenuThemeSound);
            _signalBus.Subscribe<PlayGameThemeSoundSignal>(PlayGameThemeSound);
            _signalBus.Subscribe<LostGameSignal>(PlayDefeatSound);
            _signalBus.Subscribe<WinGameSignal>(PlayVictorySound);
            _signalBus.Subscribe<TutorialCompletedSignal>(PlayVictorySound);
            _signalBus.Subscribe<PlayStartLevelSoundSignal>(PlayStartLevelSound);
            
            _signalBus.Subscribe<PlayOverButtonSoundSignal>(PlayOverButtonEffect);

            _signalBus.Subscribe<PlayCollectStarSoundSignal>(PlayCollectStarSound);
        }

        private void PlayCollectStarSound(PlayCollectStarSoundSignal obj)
        {
            _audioMixer.PlayCollectStarSoundEffect();
        }

        private void PlayClickSound(PlayClickSoundSignal obj)
        {
            _audioMixer.PlayClickSoundEffect();
        }

        private void PlayVictorSound(PlayVictorySoundSignal obj)
        {
            _audioMixer.PlayVictorSound();
        }

        private void PlayMenuThemeSound(PlayMenuThemeSoundSignal obj)
        {
            if (obj.Play)
            {
                _audioMixer.PlayInitMenuBackgroudMusic();
                return;
            }

            _audioMixer.StopInitMenuBackgroundMusic();
        }

        private void PlayGameThemeSound(PlayGameThemeSoundSignal obj)
        {
            if (obj.Play)
            {
                _audioMixer.PlayInitGameBackgroudMusic();
                return;
            }

            _audioMixer.StopInitGameBackgroundMusic();
        }

        private void PlayStartLevelSound(PlayStartLevelSoundSignal obj)
        {
            _audioMixer.PlayStartlevelSound();
        }

        private void PlayDefeatSound(LostGameSignal obj)
        {
            _audioMixer.PlayDefeatSound();
        }

        private void PlayVictorySound(WinGameSignal obj)
        {
            _audioMixer.PlayVictorSound();
        }

        private void PlayOverButtonEffect()
        {
            _audioMixer.PlayOverButtonSoundEffect();
        }

        public void SetAudioState(bool state)
        {
            _audioMixer.SetAudioState(state);
        }
    }
}