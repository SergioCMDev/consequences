﻿using System;

namespace Code.Domain.VOs
{
    [Serializable]
    public class ScenesInfoVo
    {
        public int BuildIndex;
        public string SceneName;
    }
}