﻿using System;

namespace Code.Domain.VOs
{
    [Serializable]
    public class LevelLockStatusVo
    {
        public string LevelName;
        public bool Unlocked;

        public void Unlock()
        {
            Unlocked = true;
        }
    }
}