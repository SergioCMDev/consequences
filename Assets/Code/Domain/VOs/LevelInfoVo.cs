﻿using System;

namespace Code.Domain.VOs
{
    [Serializable]
    public class LevelInfoVo
    {
        public int Id;
        public string SceneName;
        public string LevelName;
        public int StarsAtLevel;
        public int MaxMovesToPass;
        public string ImagePath;
    }
}