﻿using System;

namespace Code.Domain.VOs
{
    [Serializable]
    public class LevelPlayerVo
    {
        public string LevelName;
        public int Stars;
        public int Moves;

        public void Update(int moves, int stars)
        {
            if (Stars < stars)
            {
                Stars = stars;
            }

            Moves = moves;
        }
    }
}