﻿using Code.Enums;

namespace Code.Domain.Interfaces
{
    public interface IAudioModel
    {
        void ChangeAudioState();

        AudioState GetStateAudio();
    }
}
