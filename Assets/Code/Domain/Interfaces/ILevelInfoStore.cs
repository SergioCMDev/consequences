﻿using Code.Domain.VOs;

namespace Code.Domain.Interfaces
{
    public interface ILevelInfoStore
    {
        LevelInfoVo GetLevelInfoVo(string levelName);
        string GetLevelNameBySceneName(string sceneName);
        void GenerateBasicData();
    }
}