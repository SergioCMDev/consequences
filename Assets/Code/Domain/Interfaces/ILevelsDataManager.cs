﻿using System.Collections.Generic;
using Code.Infra.Dto;
using Code.Infra.Models;

namespace Code.Domain.Interfaces
{
    public interface ILevelsDataManager
    {
        void UnlockNextLevel();
        void SaveLevelData(LevelDto levelDataToSave);
        LevelDto GetAllLevelData(string levelName);
        List<LevelDto> GetAllLevelsData();

        LevelDto GetUnlockedLastLevel();
    }
}