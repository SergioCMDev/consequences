﻿using UnityEngine.SceneManagement;

namespace Code.Domain.Interfaces
{
    public interface ISceneManager
    {
        Scene GetActualScene();
        void ChangeToScene(string sceneName);
        void ChangeToScene(int sceneIndex);
        void RestartScene();
        void LoadNexLevel();
        void LoadTutorialScene();
        string GetNextSceneName();
        bool IsLastScene();
    }
}