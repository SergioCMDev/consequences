﻿using System.Collections.Generic;
using Code.Domain.VOs;

namespace Code.Domain.Interfaces
{
    public interface ILevelsLockStatusStore
    {
        LevelLockStatusVo GetLevelLockStatusVo(string levelName);
        bool IsLevelUnloked(string levelName);
        void GenerateBasicData(List<LevelInfoVo> infoLevels);
        void UnlockLevel(string levelName);
    }
}