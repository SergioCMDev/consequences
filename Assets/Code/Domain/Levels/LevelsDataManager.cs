﻿using System.Collections.Generic;
using Code.Domain.Interfaces;
using Code.Domain.VOs;
using Code.Infra.Dto;
using Code.Infra.Models;
using Code.Utilities;
using UnityEngine;
using Zenject;

namespace Code.Domain.Levels
{
    public class LevelsDataManager : ILevelsDataManager, IInitializable
    {
        private LevelsPlayerStore _levelsStore;
        private LevelsLockStatusStore _levelsLockStatusStore;
        private LevelInfoStore _levelInfoStore;

        //TODO CAMBIAR DE CAPA 
        [Inject] private ISceneManager _sceneManager;

        public void Initialize()
        {
            SetLevelsInfo();
            SetLevelsLockedStatus();
            SetLevelsDataPlayerInfo();
        }

        private void SetLevelsInfo()
        {
            _levelInfoStore = new LevelInfoStore();
            _levelInfoStore.GenerateBasicData();
            string jsonToSave = JsonUtility.ToJson(_levelInfoStore);
            PlayerPrefs.SetString(Constants.JsonConstants.LEVEL_INFO, jsonToSave);
            PlayerPrefs.Save();
        }

        private void SetLevelsDataPlayerInfo()
        {
            if (PlayerPrefs.HasKey(Constants.JsonConstants.LEVEL_DATA_PLAYER_INFO))
            {
                LevelsPlayerStore levelsStoreBackUp =
                    (LevelsPlayerStore) JsonUtility.FromJson(
                        PlayerPrefs.GetString(Constants.JsonConstants.LEVEL_DATA_PLAYER_INFO).ToString(),
                        typeof(LevelsPlayerStore));
                _levelsStore = new LevelsPlayerStore();
                _levelsStore.GenerateBasicData(_levelInfoStore.LevelsInfoVo);
                _levelsStore.LoadDataFromPlayerPrefs(levelsStoreBackUp.LevelsPlayerData);

                return;
            }

            _levelsStore = new LevelsPlayerStore();
            _levelsStore.GenerateBasicData(_levelInfoStore.LevelsInfoVo);

            SaveDataPlayerInfo();
        }

        private void SaveDataPlayerInfo()
        {
            string jsonToSave = JsonUtility.ToJson(_levelsStore);
            PlayerPrefs.SetString(Constants.JsonConstants.LEVEL_DATA_PLAYER_INFO, jsonToSave);
            PlayerPrefs.Save();
        }

        private void SetLevelsLockedStatus()
        {
            if (PlayerPrefs.HasKey(Constants.JsonConstants.LEVEL_LOCK_STATUS_INFO))
            {
                LevelsLockStatusStore levelsLockStatusStoreBackUp =
                    (LevelsLockStatusStore) JsonUtility.FromJson(
                        PlayerPrefs.GetString(Constants.JsonConstants.LEVEL_LOCK_STATUS_INFO).ToString(),
                        typeof(LevelsLockStatusStore));

                _levelsLockStatusStore = new LevelsLockStatusStore();
                _levelsLockStatusStore.GenerateBasicData(_levelInfoStore.LevelsInfoVo);
                _levelsLockStatusStore.LoadDataFromPlayerPrefs(levelsLockStatusStoreBackUp.LevelLockStatus);
                SaveLockStatusInfo();
                return;
            }

            _levelsLockStatusStore = new LevelsLockStatusStore();
            _levelsLockStatusStore.GenerateBasicData(_levelInfoStore.LevelsInfoVo);
            _levelsLockStatusStore.UnlockLevel("Level 1");
            SaveLockStatusInfo();
        }


        public void UnlockNextLevel()
        {
            string nextSceneName = _sceneManager.GetNextSceneName();
            if (string.IsNullOrEmpty(nextSceneName))
            {
                return;
            }
            string nextLevelName = _levelInfoStore.GetLevelNameBySceneName(nextSceneName);

            _levelsLockStatusStore.UnlockLevel(nextLevelName);

            SaveLockStatusInfo();
        }

        private void SaveLockStatusInfo()
        {
            string jsonToSave = JsonUtility.ToJson(_levelsLockStatusStore);
            PlayerPrefs.SetString(Constants.JsonConstants.LEVEL_LOCK_STATUS_INFO, jsonToSave);
            PlayerPrefs.Save();
        }


        public void SaveLevelData(LevelDto levelDataToSave)
        {
            _levelsStore.SaveLevelData(_levelInfoStore.GetLevelNameBySceneName(levelDataToSave.SceneName),
                levelDataToSave.Movements, levelDataToSave.StarsAcheived);

            SaveDataPlayerInfo();
        }

        public LevelDto GetAllLevelData(string levelName)
        {
            LevelPlayerVo levelPlayerVoStored = _levelsStore.GetLevelPlayerVo(levelName);
            LevelLockStatusVo levelLockStatusVo = _levelsLockStatusStore.GetLevelLockStatusVo(levelName);
            LevelInfoVo levelInfoVo = _levelInfoStore.GetLevelInfoVo(levelName);

            LevelDto levelDto = new LevelDto()
            {
                Id = levelInfoVo.Id,
                Movements = levelPlayerVoStored.Moves,
                StarsAcheived = levelPlayerVoStored.Stars,
                TotalStarsAtLevel = levelInfoVo.StarsAtLevel,
                SceneName = levelInfoVo.SceneName,
                LevelName = levelInfoVo.LevelName,
                Unlocked = levelLockStatusVo.Unlocked,
                ImagePath = levelInfoVo.ImagePath,
                MaxMovements = levelInfoVo.MaxMovesToPass
            };
            return levelDto;
        }

        public LevelDto GetAllLevelDataBySceneName(string sceneName)
        {
            string levelName = _levelInfoStore.GetLevelNameBySceneName(sceneName);
            return GetAllLevelData(levelName);
        }

        public List<LevelDto> GetAllLevelsData()
        {
            List<LevelDto> dataOfAllLevels = new List<LevelDto>();
            foreach (LevelInfoVo levelInfoVo in _levelInfoStore.LevelsInfoVo)
            {
                LevelDto levelDto = GetAllLevelData(levelInfoVo.LevelName);
                dataOfAllLevels.Add(levelDto);
            }

            return dataOfAllLevels;
        }

        public LevelDto GetUnlockedLastLevel()
        {
            LevelLockStatusVo lastLevelUnlocked = new LevelLockStatusVo();
            LevelDto levelInfo;

            foreach (LevelLockStatusVo levelInfoVo in _levelsLockStatusStore.LevelLockStatus)
            {
                if (levelInfoVo.Unlocked)
                {
                    lastLevelUnlocked = levelInfoVo;
                }
            }

            levelInfo = GetAllLevelData(lastLevelUnlocked.LevelName);
            return levelInfo;
        }
    }
}