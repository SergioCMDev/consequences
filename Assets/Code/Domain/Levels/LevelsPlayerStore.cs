﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Domain.VOs;

namespace Code.Domain.Levels
{
    [Serializable]
    public class LevelsPlayerStore
    {
        public List<LevelPlayerVo> LevelsPlayerData;

        public void Add(LevelPlayerVo levelplayerVoInfo)
        {
            CheckIfNull();
            if (LevelsPlayerData.Contains(levelplayerVoInfo))
            {
                return;
            }

            LevelsPlayerData.Add(levelplayerVoInfo);
        }

        private void CheckIfNull()
        {
            if (LevelsPlayerData == null)
            {
                LevelsPlayerData = new List<LevelPlayerVo>();
            }
        }

        public LevelPlayerVo GetLevelPlayerVo(string levelName)
        {
            return LevelsPlayerData.SingleOrDefault(x => x.LevelName == levelName);
        }

        public int GetMovesOfLevel(string levelName)
        {
            foreach (var level in LevelsPlayerData)
            {
                if (level.LevelName == levelName)
                {
                    return level.Moves;
                }
            }

            return 0;
        }

        public int GetStarsOfLevel(string levelName)
        {
            foreach (var level in LevelsPlayerData)
            {
                if (level.LevelName == levelName)
                {
                    return level.Stars;
                }
            }

            return 0;
        }

        public void SaveLevelData(string levelName, int moves, int stars)
        {
            LevelPlayerVo levelPlayerVoStored = GetLevelPlayerVo(levelName);
            if (levelPlayerVoStored == null)
            {
                LevelPlayerVo levelPlayerVo = new LevelPlayerVo()
                    {LevelName = levelName, Moves = moves, Stars = stars};
                Add(levelPlayerVo);
                return;
            }

            levelPlayerVoStored.Update(moves, stars);
            
        }

        public void GenerateBasicData(List<LevelInfoVo> infoLevels)
        {
            foreach (var level in infoLevels)
            {
                LevelPlayerVo levelplayerVoInfo = new LevelPlayerVo()
                    {LevelName = level.LevelName, Moves = 0, Stars = 0};
                Add(levelplayerVoInfo);
            }
        }

        public void LoadDataFromPlayerPrefs(List<LevelPlayerVo> levelsPlayerData)
        {
            foreach (var level in levelsPlayerData)
            {
                Update(level);
            }
        }
        
        private void Update(LevelPlayerVo levelLockStatusVo)
        {
            LevelPlayerVo itemToUpdate = LevelsPlayerData.Find(x => x.LevelName == levelLockStatusVo.LevelName);
            itemToUpdate.Moves = levelLockStatusVo.Moves;
            itemToUpdate.Stars = levelLockStatusVo.Stars;
        }
    }
}