﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Domain.Interfaces;
using Code.Domain.VOs;
using Zenject;

namespace Code.Domain.Levels
{
    [Serializable]
    public class LevelsLockStatusStore : ILevelsLockStatusStore
    {
        public List<LevelLockStatusVo> LevelLockStatus;

        private void Add(LevelLockStatusVo levelLockStatusVo)
        {
            CheckIfNull();
            if (LevelLockStatus.Contains(levelLockStatusVo))
            {
                return;
            }

            LevelLockStatus.Add(levelLockStatusVo);
        }

        private void CheckIfNull()
        {
            if (LevelLockStatus == null)
            {
                LevelLockStatus = new List<LevelLockStatusVo>();
            }
        }

        public LevelLockStatusVo GetLevelLockStatusVo(string levelName)
        {
            return LevelLockStatus.SingleOrDefault(x => x.LevelName == levelName);
        }

        public bool IsLevelUnloked(string levelName)
        {
            foreach (var level in LevelLockStatus)
            {
                if (level.LevelName == levelName)
                {
                    return level.Unlocked;
                }
            }

            return false;
        }

        public void GenerateBasicData(List<LevelInfoVo> infoLevels)
        {
            foreach (var level in infoLevels)
            {
                LevelLockStatusVo levelLockStatusVo = new LevelLockStatusVo()
                    {LevelName = level.LevelName, Unlocked = false};
                Add(levelLockStatusVo);
            }
        }

        public void LoadDataFromPlayerPrefs(List<LevelLockStatusVo> infoLevels)
        {
            foreach (var level in infoLevels)
            {
                Update(level);
            }
        }

        private void Update(LevelLockStatusVo levelLockStatusVo)
        {
            LevelLockStatusVo itemToUpdate = LevelLockStatus.Find(x => x.LevelName == levelLockStatusVo.LevelName);
            itemToUpdate.Unlocked = levelLockStatusVo.Unlocked;
        }

        public void UnlockLevel(string levelName)
        {
            GetLevelLockStatusVo(levelName).Unlock();
        }
    }
}