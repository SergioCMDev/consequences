﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Domain.Interfaces;
using Code.Domain.VOs;
using Code.Utilities;
using UnityEngine;

namespace Code.Domain.Levels
{
    [Serializable]
    public class LevelInfoStore : ILevelInfoStore
    {
        public List<LevelInfoVo> LevelsInfoVo;

        private void Add(LevelInfoVo levelInfoVo)
        {
            CheckIfNull();
            if (LevelsInfoVo.Contains(levelInfoVo))
            {
                return;
            }

            LevelsInfoVo.Add(levelInfoVo);
        }

        private void CheckIfNull()
        {
            if (LevelsInfoVo == null)
            {
                LevelsInfoVo = new List<LevelInfoVo>();
            }
        }

        public LevelInfoVo GetLevelInfoVo(string levelName)
        {
            return LevelsInfoVo.SingleOrDefault(x => x.LevelName == levelName);
        }
        
        public string GetLevelNameBySceneName(string sceneName)
        {
            return LevelsInfoVo.SingleOrDefault(x => x.SceneName == sceneName).LevelName;
        }

        public void GenerateBasicData()
        {
            string dataFromJson = Resources.Load(Constants.JsonConstants.PATH_OF_JSON_START_FILE_INFO).ToString();
            LevelInfoStore storeFromJson = JsonUtility.FromJson<LevelInfoStore>(dataFromJson);

            foreach (LevelInfoVo levelInfo in storeFromJson.LevelsInfoVo)
            {
                Add(levelInfo);
            }
        }
    }
}