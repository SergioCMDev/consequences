using Code.App.Controllers;
using Code.Domain.Levels;
using Zenject;

namespace Code.Domain.Installers
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            BindManagers();
        }

        private void BindManagers()
        {
            Container.BindInterfacesAndSelfTo<SceneManager>().AsSingle();
            
            Container.BindInterfacesAndSelfTo<AudioController>().AsSingle();

            Container.BindInterfacesAndSelfTo<LevelsDataManager>().AsSingle();
            
            
        }
    }
}