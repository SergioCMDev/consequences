﻿using Code.Domain.Interfaces;
using UnityEngine.SceneManagement;
using Zenject;

namespace Code.Domain.Scenes
{
    /// <summary>
    /// Esta clase debe guardar la escena actual y poder obtener la siguiente
    /// </summary>
    public class SceneManager : ISceneManager, IInitializable
    {
        private int _numScenes;
        private ScenesInfoStore scenesInfoStore;

        public void Initialize()
        {
            scenesInfoStore = new ScenesInfoStore();
            scenesInfoStore.GenerateBasicData();

            _numScenes = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
        }


        public Scene GetActualScene()
        {
            return UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        }

        public string GetNextSceneName()
        {
            int id = GetActualScene().buildIndex;
            if (id + 1 >= _numScenes)
            {
                //TODO Mostrar mensaje 
                return null;
            }

            return GetSceneNameById(id + 1);
        }

        public bool IsLastScene()
        {
            return GetNextSceneName() == null;
        }

        public void LoadNexLevel()
        {
            ChangeToScene(GetNextSceneName());
        }

        public void LoadTutorialScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tutorial");
        }
        
        private string GetSceneNameById(int id)
        {
            return scenesInfoStore.GetInfoOfScene(id).SceneName;
        }

        public void ChangeToScene(string sceneName)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
        }

        public void ChangeToScene(int sceneIndex)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneIndex);
        }

        public void RestartScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(GetActualScene().name);
        }
    }
}