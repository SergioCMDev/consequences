﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Domain.VOs;
using Code.Utilities;
using UnityEngine;

namespace Code.Domain.Scenes
{
    [Serializable]
    public class ScenesInfoStore
    {
        public List<ScenesInfoVo> ScenesInfoListVo;

        public void GenerateBasicData()
        {
            string dataFromJson =
                Resources.Load(Constants.JsonConstants.PATH_OF_JSON_SCENE_MODEL_DICTIONARY).ToString();

            ScenesInfoStore storeFromJson = JsonUtility.FromJson<ScenesInfoStore>(dataFromJson);

            foreach (ScenesInfoVo scenesInfoVo in storeFromJson.ScenesInfoListVo)
            {
                Add(scenesInfoVo);
            }
        }

        public void Add(ScenesInfoVo sceneInfoVo)
        {
            CheckIfNull();
            if (ScenesInfoListVo.Exists(x => x.SceneName == sceneInfoVo.SceneName))
            {
                return;
            }

            ScenesInfoListVo.Add(sceneInfoVo);
        }

        private void CheckIfNull()
        {
            if (ScenesInfoListVo == null)
            {
                ScenesInfoListVo = new List<ScenesInfoVo>();
            }
        }

        public ScenesInfoVo GetInfoOfScene(int buildId)
        {
            return ScenesInfoListVo.SingleOrDefault(x => x.BuildIndex == buildId);
        }

        public ScenesInfoVo GetInfoOfScene(string scenename)
        {
            return ScenesInfoListVo.SingleOrDefault(x => x.SceneName == scenename);
        }
    }
}