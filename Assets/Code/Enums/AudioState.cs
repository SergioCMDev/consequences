﻿namespace Code.Enums
{
    public enum AudioState
    {
        Enabled,
        Disabled
    }

    public enum FXAudioState
    {
        Enabled,
        Disabled
    }
}